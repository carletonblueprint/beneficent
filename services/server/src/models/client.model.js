const mongoose = require("mongoose");
const { toJSON, paginate } = require("./plugins");

const clientSchema = mongoose.Schema(
  {
    dateSigned: { type: Date, required: true },
    currentContract: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Contract",
      required: false
    },
    officer: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Officer",
      required: true
    },
    application: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Application",
      required: true
    }
  },
  {
    timestamps: true
  }
);

clientSchema.plugin(toJSON);
clientSchema.plugin(paginate);

/**
 * Check if user ID or application ID is already assigned to a client
 * @param {ObjectId} userId - The user id
 * @param {ObjectId} applicationId - The application id
 * @returns {Promise<boolean>}
 */
clientSchema.statics.isDuplicate = async function (applicationId) {
  const application = await this.findOne({ application: applicationId });
  return application;
};

/**
 * Update workload for officers
 */
clientSchema.post("save", async function () {
  await mongoose.model("Officer").updateWorkload();
});

/**
 * Update workload for officers
 */
clientSchema.post("remove", async function () {
  await mongoose.model("Officer").updateWorkload();
});

/**
 * @typedef Client
 */
const Client = mongoose.model("Client", clientSchema);

module.exports = Client;
