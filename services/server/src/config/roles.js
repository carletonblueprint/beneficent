const permissions = {
  user: ["verifyEmail"],
  admin: [
    "getUsers",
    "manageUsers",
    "getClients",
    "manageClients",
    "getApplications",
    "manageApplications",
    "getOfficers",
    "manageOfficers",
    "getContracts",
    "manageContracts",
    "verifyEmail",
    "manageDocuments"
  ],
  officer: [
    "getUsers",
    "manageUsers",
    "getClients",
    "manageClients",
    "getApplications",
    "manageApplications",
    "getContracts",
    "getOfficers",
    "manageContracts",
    "verifyEmail",
    "manageDocuments"
  ]
};

const roles = Object.keys(permissions);
const roleRights = new Map(Object.entries(permissions));

module.exports = {
  roles,
  roleRights
};
