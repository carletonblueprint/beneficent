import { cleanup, render } from "@testing-library/react";
import * as React from "react";
import LoanOfficerDropdown from "../components/common/LoanOfficerDropdown/LoanOfficerDropdown";
import avatar from "../assets/avatar.png";

afterEach(cleanup);

function createLoanOfficer(name, avatar, status) {
  return { name: name, avatar: avatar, status: status };
}

const status = { good: 0, okay: 1, bad: 2 };
const mockLoanOfficers = [
  createLoanOfficer("Tasha", avatar, status.good),
  createLoanOfficer("Sarah", avatar, status.okay),
  createLoanOfficer("Thang", avatar, status.bad),
  createLoanOfficer("Name Name", avatar, status.bad)
];

const mockNoValue: string = "";
const mockValue: string = "Sarah";

it("matches snapshot", () => {
  const { asFragment } = render(
    <LoanOfficerDropdown loanOfficers={mockLoanOfficers} value={mockNoValue}></LoanOfficerDropdown>
  );

  expect(asFragment()).toMatchSnapshot();
});

it("matches snapshot when given default value", () => {
  const { asFragment } = render(
    <LoanOfficerDropdown loanOfficers={mockLoanOfficers} value={mockValue}></LoanOfficerDropdown>
  );

  expect(asFragment()).toMatchSnapshot();
});
