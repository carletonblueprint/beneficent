const httpStatus = require("http-status");
const { Application, User } = require("../models");
const userService = require("./user.service");
const ApiError = require("../utils/ApiError");
const { validateOfficerId, validateUserId, validateAcknowledgements } = require("../utils/serviceUtils");
const officerService = require("./officer.service");
const clientService = require("./client.service");

/**
 * Create a application
 * @param {Object} applicationBody
 * @returns {Promise<Application>}
 */
const createApplication = async (applicationBody) => {
  await validateAcknowledgements(applicationBody.acknowledgements, "Acknowledgements not met");

  if (applicationBody.user) {
    await validateUserId(applicationBody.user, "User with this ID does not exist");
    const user = await User.findById(applicationBody.user);
    if (
      user.email != applicationBody.email ||
      user.firstName != applicationBody.firstName ||
      user.lastName != applicationBody.lastName
    ) {
      throw new ApiError(httpStatus.BAD_REQUEST, "Application name and/or email does not match with user name and/or email");
    }
  }
  if (applicationBody.officer) {
    await validateOfficerId(applicationBody.officer, "Officer with this ID does not exist");
  }
  if (await Application.isDuplicate(applicationBody.email)) {
    throw new ApiError(httpStatus.BAD_REQUEST, "Application with this email already exists");
  }
  return Application.create(applicationBody);
};

/**
 * Query for applications
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.populate] - Populate data fields. Hierarchy of fields should be separated by (.). Multiple populating criteria should be separated by commas (,)
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryApplications = async (filter, options) => {
  let applicationFilter = { $and: [{ $or: [] }], $or: [] };

  if (filter.nameOrEmail) {
    applicationFilter.$and[0].$or.push({ firstName: { $regex: filter.nameOrEmail, $options: "i" } });
    applicationFilter.$and[0].$or.push({ lastName: { $regex: filter.nameOrEmail, $options: "i" } });
    applicationFilter.$and[0].$or.push({ email: { $regex: filter.nameOrEmail, $options: "i" } });
  }

  if (filter.status) applicationFilter.$or.push({ status: filter.status });
  if (!filter.displayAll) applicationFilter.$and.push({ status: { $nin: ["Active Client", "Archived"] } });
  if (filter.user) applicationFilter.$and.push({ user: filter.user });
  if (filter.officer) applicationFilter.$or.push({ officer: filter.officer });

  if (applicationFilter.$and[0].$or.length === 0) applicationFilter.$and.shift();
  if (applicationFilter.$and.length === 0) delete applicationFilter.$and;
  if (applicationFilter.$or.length === 0) delete applicationFilter.$or;

  const applications = await Application.paginate(applicationFilter, options);
  if (applications.results.length == 0) {
    throw new ApiError(httpStatus.NOT_FOUND, "No applications found");
  }
  return applications;
};

/**
 * Get application by id
 * @param {ObjectId} applicationId
 * @returns {Promise<Application>}
 */
const getApplicationById = async (applicationId) => {
  return Application.findById(applicationId);
};

/**
 * Get application by user id
 * @param {ObjectId} userId
 * @returns {Promise<Application>}
 */
const getApplicationByUserId = async (userId) => {
  return Application.findOne({ user: userId });
};

/**
 * Update application by id
 * @param {ObjectId} applicationId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateApplicationById = async (applicationId, updateBody) => {
  const application = await getApplicationById(applicationId);
  if (!application) {
    throw new ApiError(httpStatus.NOT_FOUND, "Application not found");
  }

  if (updateBody.user) {
    const user = await userService.getUserById(updateBody.user);
    if (!user) {
      throw new ApiError(httpStatus.NOT_FOUND, "User not found");
    }
  }

  if (updateBody.officer) {
    const officer = await officerService.getOfficerById(updateBody.officer);
    if (!officer) {
      throw new ApiError(httpStatus.NOT_FOUND, "Officer not found");
    }
  }

  if (updateBody.status && !application.officer) {
    throw new ApiError(httpStatus.BAD_REQUEST, "Cannot update a status with no assigned officer");
  }

  if (updateBody.status === "Active Client") {
    await clientService.createClient({
      dateSigned: Date.now(),
      application: application._id,
      officer: application.officer
    });
  }

  Object.assign(application, updateBody);
  await application.save();
  return application;
};

/**
 * Delete application by id
 * @param {ObjectId} applicationId
 * @returns {Promise<User>}
 */
const deleteApplicationById = async (applicationId) => {
  const application = await getApplicationById(applicationId);
  if (!application) {
    throw new ApiError(httpStatus.NOT_FOUND, "Application not found");
  }
  await application.remove();
  return application;
};

module.exports = {
  createApplication,
  queryApplications,
  getApplicationById,
  getApplicationByUserId,
  updateApplicationById,
  deleteApplicationById
};
