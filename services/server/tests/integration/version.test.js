const request = require("supertest");
const httpStatus = require("http-status");
const app = require("../../src/app");
const { version } = require("../../package.json");

describe("VERSION route", () => {
  describe("GET /api/v1/version", () => {
    test("should return 200 and the version", async () => {
      const res = await request(app).get("/api/v1/version").expect(httpStatus.OK);
      expect(res.body).toEqual({
        version: version
      });
    });
  });
});
