const mongoose = require("mongoose");
const faker = require("faker");
const config = require("../config/config");
const { Application, Client, Contract, Officer, User } = require("../models");
const { applicationService, contractService } = require("../services");
const getRandomPhoneNumber = require("../../tests/fixtures/phoneNumber.fixture");

const validStatuses = [
  "Received",
  "Assigned",
  "Contacted",
  "Interview",
  "Requested Information",
  "Proccessing Information",
  "Committee Discussion",
  "Accepted",
  "Rejected",
  "Waitlisted",
  "Contract Sent",
  "Contract Signed",
  "Active Client",
  "Archived"
];

const seedDatabase = async () => {
  await truncateDatabase();
  await createUsers();
  await createOfficersAndAdmin();
  await createApplications();
  await updateApplicationRoles();
  await createContracts();
};

const truncateDatabase = async () => {
  await Application.deleteMany();
  await Client.deleteMany();
  await Contract.deleteMany();
  await Officer.deleteMany();
  await User.deleteMany();
};

const createUsers = async () => {
  await User.create({
    firstName: "fake first name",
    lastName: "fake last name",
    phoneNumber: "+16132332892",
    address: "'123 Melon Street, Ottawa, K1L6L3'",
    country: "Canada",
    email: "fake@example.com",
    password: "password1"
  });
  listOfUsers = [];
  for (let i = 0; i < 8; i++) {
    const currentUserInfo = {
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      phoneNumber: getRandomPhoneNumber(),
      address: faker.address.streetAddress(),
      country: faker.address.country(),
      email: faker.internet.email(),
      password: "dh3298y32hon32"
    };
    listOfUsers.push(currentUserInfo);
  }
  await User.create(listOfUsers);
};

const createOfficersAndAdmin = async () => {
  const users = await User.find();
  for (let i = 0; i < 3; i++) {
    const toUpdate = await User.findById(users[i]._id);
    if (i === 0) {
      toUpdate.role = "admin";
      await toUpdate.save();
    } else {
      toUpdate.role = "officer";
      await toUpdate.save();
      await Officer.create([{ user: toUpdate._id }]);
    }
  }
};

const createApplications = async () => {
  const users = await User.find();
  const date = new Date("2001-01-21");
  const officer1 = await Officer.findOne({ user: users[1]._id });
  const officer2 = await Officer.findOne({ user: users[2]._id });

  for (let i = 3; i < users.length; i++) {
    const newApplication = {
      firstName: users[i].firstName,
      lastName: users[i].lastName,
      email: users[i].email,
      phoneNumber: users[i].phoneNumber,
      address: users[i].address,
      city: faker.address.cityName(),
      province: faker.address.state(),
      sex: faker.name.gender(),
      dateOfBirth: date,
      maritalStatus: "Single",
      citizenship: users[i].country,
      preferredLanguage: "English",
      employmentStatus: "Employed Full Time",
      loanAmountRequested: faker.datatype.number(),
      loanType: "Other",
      debtCircumstances: faker.lorem.words(),
      recommendationInfo: faker.lorem.words(),
      acknowledgements: {
        loanPurpose: true,
        maxLoan: true,
        repayment: true,
        residence: true,
        netPositiveIncome: true,
        guarantorConsent: true
      },
      emailOptIn: true,
      status: "Received",
      officer: i < 6 ? officer1._id : officer2._id,
      preferredLanguage: "english",
      emailOptIn: true,
      acknowledgements: {
        loanPurpose: true,
        maxLoan: true,
        residence: true,
        netPositiveIncome: true,
        guarantorConsent: true,
        repayment: true
      },
      guarantor: {
        hasGuarantor: true,
        fullName: faker.name.findName(),
        email: faker.internet.email().toLowerCase(),
        phoneNumber: getRandomPhoneNumber()
      },
      user: users[i]._id
    };
    await applicationService.createApplication(newApplication);
  }
};

const updateApplicationRoles = async () => {
  const applications = await Application.find();
  for (let i = 0; i < applications.length; i++) {
    let updateBody = {};
    if (faker.datatype.boolean()) {
      updateBody.status = "Active Client";
    } else {
      updateBody.status = validStatuses[Math.floor(Math.random() * validStatuses.length)];
    }
    await applicationService.updateApplicationById(applications[i]._id, updateBody);
  }
};

const createContracts = async () => {
  const clients = await Client.find();
  const date1 = new Date("2001-01-21");
  const date2 = new Date("2002-01-21");
  const date3 = new Date("2001-01-01");
  for (let i = 0; i < clients.length; i++) {
    const newContract = {
      guarantorName: "N/A",
      approvedLoanAmount: faker.datatype.number(),
      firstPaymentDue: date1,
      finalPaymentDue: date2,
      status: "Draft",
      contractStartDate: date3,
      client: clients[i]._id
    };
    const currContract = await contractService.createContract(newContract);
    clients[i].currentContract = currContract;
    await clients[i].save();
  }
};

mongoose.connect(config.mongoose.url, config.mongoose.options).then(async (db) => {
  await seedDatabase();
  db.disconnect();
});

module.exports = seedDatabase;
