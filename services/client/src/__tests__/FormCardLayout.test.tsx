import * as React from "react";
import { cleanup, render } from "@testing-library/react";

import FormCardLayout from "../components/common/FormCardLayout/FormCardLayout";

afterEach(cleanup);

describe("Form Card Layout", () => {
  it("render correctly", () => {
    const { getByTestId } = render(
      <FormCardLayout>
        <div className="testing-className" />
      </FormCardLayout>
    );
    expect(getByTestId("form-panel").childNodes[0]).toHaveClass("testing-className");
  });

  it("rendering matches snapshot", () => {
    const { asFragment } = render(
      <FormCardLayout>
        <div />
      </FormCardLayout>
    );
    expect(asFragment()).toMatchSnapshot();
  });
});
