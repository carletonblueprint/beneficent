import { AxiosError, AxiosResponse } from "axios";
import { apiProvider } from "./provider";
import { GetModel, UpdateModel, CreateModel, UploadModel } from "./types";

export class ApiCore {
  getAll: (token: string, model?: GetModel) => Promise<AxiosResponse<any>> | Promise<AxiosError<any>>;
  getSingle: (token: string, id: string) => Promise<AxiosResponse<any>> | Promise<AxiosError<any>>;
  post: (token: string, model: CreateModel) => Promise<AxiosResponse<any>> | Promise<AxiosError<any>>;
  patch: (token: string, id: string, model: UpdateModel) => Promise<AxiosResponse<any>> | Promise<AxiosError<any>>;
  remove: (token: string, id: string) => Promise<AxiosResponse<any>> | Promise<AxiosError<any>>;
  upload: (token: string, model: UploadModel) => Promise<AxiosResponse<any>> | Promise<AxiosError<any>>;
  download: (token: string, id: string) => Promise<AxiosResponse<any>> | Promise<AxiosError<any>>;
  generateHeaders: (token: string) => any;

  constructor(options) {
    this.getAll = (token: string, model?: GetModel): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
      return apiProvider.getAll(options.url, token, model);
    };

    this.getSingle = (token: string, id: string): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
      return apiProvider.getSingle(options.url, token, id);
    };

    this.post = (token: string, model: CreateModel): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
      return apiProvider.post(options.url, token, model);
    };

    this.patch = (token: string, id: string, model: UpdateModel): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
      return apiProvider.patch(options.url, token, id, model);
    };

    this.remove = (token: string, id: string): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
      return apiProvider.remove(options.url, token, id);
    };

    this.generateHeaders = (token: string) => {
      return apiProvider.generateHeaders(token);
    };

    this.upload = (token: string, model: UploadModel): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
      return apiProvider.upload(options.url, token, model);
    };

    this.download = (token: string, id: string): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
      return apiProvider.download(options.url, token, id);
    };
  }
}
