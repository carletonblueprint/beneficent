import React from "react";
import "./ApplicationsTable.scss";
import {
  createMuiTheme,
  FormControl,
  IconButton,
  InputBase,
  MenuItem,
  MuiThemeProvider,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  withStyles,
  Divider
} from "@material-ui/core";
import PropTypes from "prop-types";
import { KeyboardArrowLeft, KeyboardArrowRight, ExpandMore, ExpandLess } from "@material-ui/icons";
import StatusDropdown from "../../common/StatusDropdown/StatusDropdown";
import LoanOfficerDropdown from "../../common/LoanOfficerDropdown/LoanOfficerDropdown";
import axios, { AxiosRequestConfig } from "axios";
import avatar from "../../../assets/avatar.png";
import moment from "moment";
import MyTable, { ColumnType } from "../../common/Table/Table";

/**
 * Create Loan Officer from input data
 */
function createLoanOfficer(name, av, stat) {
  return { name: name, avatar: av, status: stat };
}

/**
 * Filler Loan Officers for Applications Table -- data to be pulled from backend
 */
// TODO - find the real status descriptions from backend
const STATUS = { good: 0, okay: 1, bad: 2 };
const loanOfficers = [
  createLoanOfficer("Tasha", avatar, STATUS.good),
  createLoanOfficer("Sarah", avatar, STATUS.okay),
  createLoanOfficer("Thang", avatar, STATUS.bad),
  createLoanOfficer("Name Name", avatar, STATUS.bad)
];

/**
 * Customized Drop Down Input for Status field
 */
const StatusInput = withStyles(() => ({
  input: {
    width: 127,
    height: 16,
    border: "1px solid rgb(0, 0, 0, 0.12)",
    borderRadius: 4,
    fontSize: 16,
    color: "black",
    paddingTop: 6,
    paddingLeft: 22,
    paddingBottom: 10
  }
}))(InputBase);

/**
 * Method to retrieve all applications from database
 */
function getApplications(accessToken, page, limit): Promise<any> {
  const options: AxiosRequestConfig = {
    url: `${process.env.REACT_APP_API_ENDPOINT}/api/v1/applications`,
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${accessToken}`
    }
  };

  return axios(options).then((res) => res.data);
}

type ApplicationsTableProps = {
  accessToken: string;
};

const ApplicationsTable: React.FC<ApplicationsTableProps> = ({ accessToken }) => {
  const [totalRows, setTotalRows] = React.useState(0);
  const [data, setData] = React.useState([]);
  const [page, setPage] = React.useState(1);
  const perPage = 10;

  const onPaginate = (newPage) => {
    setPage(page);
  };

  // Load applications from database for page 1
  React.useEffect(() => {
    getApplications(accessToken, page, perPage).then((res) => {
      setData(res.results);
      setTotalRows(res.totalResults);
    });
  }, [page]);

  const columns: ColumnType<any>[] = [
    {
      title: "Name",
      render: (record) => record.name
    },
    {
      title: "Date Applied",
      render: (record) => moment(record.dateApplied).format("MMM D, YYYY")
    },
    {
      title: "Status",
      render: (record) => <StatusDropdown value={record.status} accessToken={accessToken} applicationId={record.id} />
    },
    {
      title: "Assigned To",
      render: (record) => <LoanOfficerDropdown loanOfficers={loanOfficers} value={record.loanOfficer} />
    }
  ];

  return (
    <MyTable
      data={data}
      columns={columns}
      pagination={{
        perPage,
        onPaginate,
        totalRows,
        page: page - 1
      }}
    />
  );
};

export default ApplicationsTable;
