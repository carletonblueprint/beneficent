const mongoose = require("mongoose");
const Officer = require("../../src/models/officer.model");
const { officerUserOne, officerUserTwo } = require("./user.fixture");

const officerOne = {
  _id: mongoose.Types.ObjectId(),
  user: officerUserOne._id
};

const officerTwo = {
  _id: mongoose.Types.ObjectId(),
  user: officerUserTwo._id
};

const insertOfficers = async (officers) => {
  await Officer.insertMany(officers.map((officer) => ({ ...officer })));
};

module.exports = {
  insertOfficers,
  officerOne,
  officerTwo
};
