import React from "react";
import PropTypes from "prop-types";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogContent from "@material-ui/core/DialogContent";
import Dialog from "@material-ui/core/Dialog";
import { IconButton } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";

const Title = (props) => {
  const { title } = props;
  const onClose = () => {
    props.onClose();
  };

  return (
    <DialogTitle id="simple-dialog-title">
      {title}
      {onClose ? (
        <IconButton aria-label="close" onClick={onClose} style={{ position: "absolute", right: 8, top: 8 }}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
};

Dialogue.propTypes = {
  onClose: PropTypes.func,
  open: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  children: PropTypes.any
};

export default function Dialogue(props) {
  const { title, text, open, children } = props;
  const [isOpen, setOpen] = React.useState(open);

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={isOpen} maxWidth={false}>
      <Title title={title} onClose={handleClose} />
      <DialogContent>
        <DialogContentText>{text}</DialogContentText>
      </DialogContent>
      {children}
    </Dialog>
  );
}
