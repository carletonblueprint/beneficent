export interface createOfficer {
  user: string;
}

export interface getOfficers {
  firstName?: string;
  lastName?: string;
  email?: string;
  user?: string;
  populate?: string;
  sortBy?: string;
  limit?: number;
  page?: number;
}

export interface updateOfficer {
  user: string;
}
