import React from "react";
import "./StatusDropdown.scss";
import { FormControl, MenuItem, Select, withStyles, InputBase, Divider } from "@material-ui/core";
import { ExpandMore, ExpandLess } from "@material-ui/icons";
import axios, { AxiosRequestConfig } from "axios";

/**
 * Customized Drop Down Input for Status field
 */
const StatusInput = withStyles(() => ({
  input: {
    width: 127,
    height: 16,
    border: "1px solid rgb(0, 0, 0, 0.12)",
    borderRadius: 4,
    fontSize: 16,
    color: "black",
    paddingTop: 6,
    paddingLeft: 22,
    paddingBottom: 10
  }
}))(InputBase);

type ApplicationsTableProps = {
  accessToken: string;
  applicationId: string;
};

/**
 * Method to update application in database
 */
function updateApplication(accessToken, applicationId, applicationStatus) {
  const options: AxiosRequestConfig = {
    url: `${process.env.REACT_APP_API_ENDPOINT}/api/v1/applications/${applicationId}`,
    method: "PATCH",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${accessToken}`
    },
    data: {
      status: applicationStatus
    }
  };

  axios(options).then((res) => res.data);
}

/**
 * Renders Application Status dropdown to update the status of an
 *  application
 */
function StatusDropdown(props) {
  const [status, setStatus] = React.useState(props.value);

  const handleChangeStatus = (event) => {
    setStatus(event.target.value);
    // TODO: update on backend database

    updateApplication(props.accessToken, props.applicationId, status);
  };

  const applicationStatus = [
    "Received",
    "Assigned",
    "Contacted",
    "Interview",
    "Requested Information",
    "Received Information",
    "Processing Information",
    "Committee Discussion",
    "Accepted",
    "Rejected",
    "Contract Sent",
    "Contract Signed",
    "Active Client",
    "Archived"
  ];

  return (
    <FormControl variant="outlined">
      <Select
        labelId="status-select-label"
        id="status-select"
        value={status}
        onChange={handleChangeStatus}
        input={<StatusInput />}
        inputProps={{
          classes: {
            icon: "dropdown-icon"
          }
        }}
        IconComponent={ExpandMore}
      >
        <div className="status-dropdown">
          <p>Status</p>
          <ExpandLess className="icon" />
        </div>
        <Divider />
        {applicationStatus.map((appStatus) => (
          <MenuItem value={appStatus} key={appStatus}>
            <p className="status-menu-item">{appStatus}</p>
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}

export default StatusDropdown;
