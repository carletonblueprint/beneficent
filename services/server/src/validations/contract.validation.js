const Joi = require("joi");
const { objectId, contractFileExtension } = require("./custom.validation");

const createContract = {
  body: Joi.object().keys({
    guarantorName: Joi.string().required(),
    approvedLoanAmount: Joi.number().required(),
    firstPaymentDue: Joi.date().required(),
    finalPaymentDue: Joi.date().required(),
    monthlyPayment: Joi.number(),
    finalPayment: Joi.number(),
    status: Joi.string().valid("Draft", "Active", "Archived").required(),
    contractStartDate: Joi.date().required(),
    client: Joi.string().required().custom(objectId)
  })
};

const getContracts = {
  query: Joi.object().keys({
    client: Joi.string().custom(objectId),
    officer: Joi.string().custom(objectId),
    application: Joi.string().custom(objectId),
    populate: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getContract = {
  params: Joi.object().keys({
    contractId: Joi.required().custom(objectId)
  })
};

const updateContract = {
  params: Joi.object().keys({
    contractId: Joi.required().custom(objectId)
  }),
  body: Joi.object()
    .keys({
      guarantorName: Joi.string(),
      approvedLoanAmount: Joi.number(),
      firstPaymentDue: Joi.date(),
      finalPaymentDue: Joi.date(),
      monthlyPayment: Joi.number(),
      finalPayment: Joi.number(),
      contractStartDate: Joi.date(),
      client: Joi.string().custom(objectId),
      user: Joi.string().custom(objectId)
    })
    .min(1)
};

const deleteContract = {
  params: Joi.object().keys({
    contractId: Joi.required().custom(objectId)
  })
};

const downloadContract = {
  params: Joi.object().keys({
    contractId: Joi.required().custom(objectId)
  })
};

const uploadContract = {
  file: Joi.object().keys({
    originalName: Joi.string().required().custom(contractFileExtension),
    fieldName: Joi.string().required(),
    encoding: Joi.string().required(),
    mimeType: Joi.string().required(),
    buffer: Joi.binary().required(),
    size: Joi.number()
  }),
  params: Joi.object().keys({
    contractId: Joi.required().custom(objectId)
  })
};

module.exports = {
  createContract,
  getContracts,
  getContract,
  updateContract,
  deleteContract,
  downloadContract,
  uploadContract
};
