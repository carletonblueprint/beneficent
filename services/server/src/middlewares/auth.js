const passport = require("passport");
const httpStatus = require("http-status");
const ApiError = require("../utils/ApiError");
const { roleRights } = require("../config/roles");
const { clientService, applicationService } = require("../services");

const userAuth = (req, user) => {
  if (req.method === "GET" || req.method === "PATCH") {
    return req.params.userId !== user.id;
  }
  return true;
};

const clientAuth = async (req, user) => {
  const client = await clientService.getClientByUserId(user.id);
  if (client && req.method === "GET") {
    return req.params.clientId !== client.id;
  }
  return true;
};

const applicationAuth = async (req, user) => {
  const application = await applicationService.getApplicationByUserId(user.id);
  if (application && req.method === "GET") {
    return req.params.applicationId !== application.id;
  }
  return true;
};

const verifyCallback = (req, resolve, reject, requiredRights) => async (err, user, info) => {
  if (err || info || !user) {
    return reject(new ApiError(httpStatus.UNAUTHORIZED, "Please authenticate"));
  }
  req.user = user;
  if (requiredRights.length) {
    const userRights = roleRights.get(user.role);
    const hasRequiredRights = requiredRights.every((requiredRight) => userRights.includes(requiredRight));
    if (!hasRequiredRights && userAuth(req, user) && (await clientAuth(req, user)) && (await applicationAuth(req, user))) {
      return reject(new ApiError(httpStatus.FORBIDDEN, "Forbidden"));
    }
  }
  resolve();
};

const auth =
  (...requiredRights) =>
  async (req, res, next) => {
    return new Promise((resolve, reject) => {
      passport.authenticate("jwt", { session: false }, verifyCallback(req, resolve, reject, requiredRights))(req, res, next);
    })
      .then(() => next())
      .catch((err) => next(err));
  };

module.exports = auth;
