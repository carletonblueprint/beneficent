import { ApiCore } from "../utilities/core";

export const apiClients = new ApiCore({
  getAll: true,
  getSingle: true,
  post: true,
  patch: true,
  delete: true,
  upload: false,
  download: false,
  generateHeaders: true,
  url: "clients"
});
