import { cleanup, render, act, fireEvent } from "@testing-library/react";
import * as React from "react";
import CreateEditContract from "../components/ApplicationsLayout/ApplicationDetails/CreateEditContract/CreateEditContract";

afterEach(cleanup);

const mockProps = {
  guarantor: "guarantor",
  officerId: "officerId",
  userId: "userId"
};

describe("Create Edit Contract Form", () => {
  it("matches snapshot", () => {
    const { asFragment } = render(<CreateEditContract {...mockProps} />);

    expect(asFragment()).toMatchSnapshot();
  });

  describe("Changing text fields", () => {
    it("should correctly change amount fields", () => {
      const { getByTestId } = render(<CreateEditContract {...mockProps} />);
      const loanAmountInput: any = getByTestId("loan-amount");
      const monthlyInstallmentInput: any = getByTestId("monthly-installment");
      const finalInstallmentInput: any = getByTestId("final-installment");

      act(() => {
        fireEvent.change(loanAmountInput, {
          target: { value: 10 }
        });
        fireEvent.change(monthlyInstallmentInput, {
          target: { value: 20 }
        });
        fireEvent.change(finalInstallmentInput, {
          target: { value: 30 }
        });
      });

      expect(loanAmountInput.value).toBe("10");
      expect(monthlyInstallmentInput.value).toBe("20");
      expect(finalInstallmentInput.value).toBe("30");
    });

    it("should correctly change contract date field", () => {
      const { getByTestId } = render(<CreateEditContract {...mockProps} />);
      const contractDateInput: any = getByTestId("contract-date");
      const firstInstallmentDueInput: any = getByTestId("first-installment-due");
      const lastInstallmentDueInput: any = getByTestId("last-installment-due");

      act(() => {
        fireEvent.change(contractDateInput, {
          target: { value: 20000101 } // Input in number for date
        });
        fireEvent.change(firstInstallmentDueInput, {
          target: { value: "2a0b0c0d0e2f0g1h" } // Input has letters
        });
        fireEvent.change(lastInstallmentDueInput, {
          target: { value: "2!0@0#0/0%a3/0^1" } // Input has special numbers
        });
      });

      expect(contractDateInput.value).toBe("2000/01/01");
      expect(firstInstallmentDueInput.value).toBe("2000/02/01");
      expect(lastInstallmentDueInput.value).toBe("2000/03/01");
    });
  });

  describe("form validation", () => {
    it("should show error for submitting improper dates", async () => {
      const { getByTestId } = render(<CreateEditContract {...mockProps} />);
      const contractDateInput: any = getByTestId("contract-date");
      const firstInstallmentDueInput: any = getByTestId("first-installment-due");
      const lastInstallmentDueInput: any = getByTestId("last-installment-due");
      const contractForm = getByTestId("contract-form");

      await act(async () => {
        await fireEvent.change(contractDateInput, {
          target: { value: "" }
        });

        await fireEvent.change(firstInstallmentDueInput, {
          target: { value: "2000/12/" } // Input has special numbers
        });

        await fireEvent.change(lastInstallmentDueInput, {
          target: { value: "1125215098/1251/125" } // Input has special numbers
        });

        await fireEvent.submit(contractForm);
      });

      expect(contractDateInput).toHaveAttribute("aria-invalid", "true");
      expect(firstInstallmentDueInput).toHaveAttribute("aria-invalid", "true");
      expect(lastInstallmentDueInput).toHaveAttribute("aria-invalid", "true");
    });

    it("should show error for submitting empty loan fields", async () => {
      const { getByTestId } = render(<CreateEditContract {...mockProps} />);
      const loanAmountInput: any = getByTestId("loan-amount");
      const monthlyInstallmentInput: any = getByTestId("monthly-installment");
      const finalInstallmentInput: any = getByTestId("final-installment");
      const contractForm = getByTestId("contract-form");

      await act(async () => {
        await fireEvent.submit(contractForm);
      });

      expect(loanAmountInput).toHaveAttribute("aria-invalid", "true");
      expect(monthlyInstallmentInput).toHaveAttribute("aria-invalid", "true");
      expect(finalInstallmentInput).toHaveAttribute("aria-invalid", "true");
    });
  });
});
