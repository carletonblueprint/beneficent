const faker = require("faker");
const mongoose = require("mongoose");
const { Client } = require("../../../src/models");
const { dateOne } = require("../../fixtures/client.fixture");

describe("Client model", () => {
  describe("Client validation", () => {
    let newClient;
    beforeEach(() => {
      newClient = {
        dateSigned: dateOne,
        officer: mongoose.Types.ObjectId(),
        application: mongoose.Types.ObjectId()
      };
    });

    test("should correctly validate a valid client", async () => {
      await expect(new Client(newClient).validate()).resolves.toBeUndefined();
    });

    test("should throw a validation error if date signed is invalid", async () => {
      newClient.dateSigned = "invalidDate";
      await expect(new Client(newClient).validate()).rejects.toThrow();
    });

    test("should throw a validation error if loan officer is invalid", async () => {
      newClient.officer = "invalidLoanOfficer";
      await expect(new Client(newClient).validate()).rejects.toThrow();
    });

    test("should throw a validation error if application is invalid", async () => {
      newClient.application = "invalidApplication";
      await expect(new Client(newClient).validate()).rejects.toThrow();
    });
  });
});
