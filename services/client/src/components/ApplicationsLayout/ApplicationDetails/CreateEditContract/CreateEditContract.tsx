import React, { forwardRef, useImperativeHandle, useRef } from "react";
import { Formik, FormikProps } from "formik";
import * as Yup from "yup";
import { InputAdornment, makeStyles, Grid, TextField, Card, CardHeader, CardContent, Typography } from "@material-ui/core";
import NumberFormat from "react-number-format";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";

import "./CreateEditContract.scss";
import { FormikSubmitObject } from "../../ApplicationsLayout";
import axios, { AxiosRequestConfig } from "axios";

interface NumberFormatProps {
  inputRef: (instance: NumberFormat | null) => void;
  onChange: (event: { target: { name: string; value: string } }) => void;
  name: string;
}

const CurrencyFormat = (props: NumberFormatProps) => {
  const { inputRef, onChange, ...other } = props;

  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        onChange({
          target: {
            name: props.name,
            value: values.value
          }
        });
      }}
      thousandSeparator
      isNumericString
      isAllowed={(values) => {
        const { formattedValue, floatValue } = values;
        return Boolean(formattedValue === "" || (floatValue && floatValue <= 99999));
      }}
    />
  );
};

const errorUseStyles = makeStyles((theme) => ({
  root: {
    "&$error": {
      top: 55,
      position: "absolute"
    }
  },
  error: {}
}));

interface FormikValuesType {
  loanAmount: number | undefined;
  contractDate: Date | null;
  monthlyInstallment: number | undefined;
  finalInstallment: number | undefined;
  firstInstallmentDue: Date | null;
  lastInstallmentDue: Date | null;
}

interface CreateEditContractProps {
  guarantor: string | undefined;
  officerId: string | undefined;
  userId: string | undefined;
}

const CreateEditContract = forwardRef<FormikSubmitObject, CreateEditContractProps>((props, ref) => {
  const token = localStorage.getItem("accessToken");
  const { guarantor, officerId, userId } = props;
  const errorTextStyles = errorUseStyles();
  const initialValues: FormikValuesType = {
    loanAmount: undefined,
    contractDate: null,
    monthlyInstallment: undefined,
    finalInstallment: undefined,
    firstInstallmentDue: null,
    lastInstallmentDue: null
  };
  const formikRef = useRef<FormikProps<FormikValuesType>>(null);

  useImperativeHandle(
    ref,
    () => ({
      handleSubmit: () => formikRef.current?.handleSubmit()
    }),
    []
  );

  return (
    <Card className="create-edit-contract-card">
      <CardHeader title="Loan Details">Loan Details</CardHeader>
      <CardContent>
        <Formik
          innerRef={formikRef}
          initialValues={initialValues}
          onSubmit={(values, { setSubmitting }) => {
            // TODO: Continue on hooking up API in this function
            const formData = {
              guarantorName: guarantor,
              approvedLoanAmount: values.loanAmount,
              firstPaymentDue: values.firstInstallmentDue,
              finalPaymentDue: values.lastInstallmentDue,
              monthlyPayment: values.monthlyInstallment,
              finalPayment: values.finalInstallment,
              officer: officerId,
              user: userId
            };

            const options: AxiosRequestConfig = {
              url: `${process.env.REACT_APP_API_ENDPOINT}/api/v1/contracts`,
              method: "post",
              headers: {
                "Content-Type": "multipart/form-data",
                Authorization: `Bearer ${token}`
              },
              data: formData
            };

            axios(options)
              .then((res) => {
                // good response
                console.log(res);
              })
              .catch((err) => {
                console.log(err.message);
              });
          }}
          validationSchema={Yup.object().shape({
            loanAmount: Yup.number()
              .typeError("Loan amount is required")
              .test("Check empty input", "Loan amount is required", (value) => value !== undefined)
              .test("Check amount is positive", "The amount must be greater than 0", (value) => value! > 0),
            contractDate: Yup.date().nullable().typeError("Please enter a valid date").required("Contract date is required"),
            monthlyInstallment: Yup.number()
              .typeError("Monthly installment is required")
              .test("Check empty input", "Monthly installment is required", (value) => value !== undefined)
              .test(
                "Check monthly installment is positive",
                "Monthly installment must be greater than 0",
                (value) => value! > 0
              ),
            finalInstallment: Yup.number()
              .typeError("Final installment is required")
              .test("Check empty input", "Final installment is required", (value) => value !== undefined)
              .test(
                "Check final installment is positive",
                "The final installment must be greater than 0",
                (value) => value! > 0
              ),
            firstInstallmentDue: Yup.date()
              .nullable()
              .typeError("Please enter a valid date")
              .required("First installment due date is required"),
            lastInstallmentDue: Yup.date()
              .nullable()
              .typeError("Please enter a valid date")
              .required("Last installment due date is required")
          })}
        >
          {(props) => {
            const { values, touched, errors, handleSubmit, setFieldValue } = props;
            return (
              <MuiPickersUtilsProvider utils={MomentUtils}>
                <form data-testid="contract-form" onSubmit={handleSubmit}>
                  <Grid container direction="row" spacing={10} style={{ width: "850px", minWidth: "1000px" }}>
                    <Grid item container direction="column" spacing={5} xs={6}>
                      <Grid item>
                        <Typography variant="body2" className="field-label">
                          Approved Loan Amount
                        </Typography>
                        <TextField
                          inputProps={{ "data-testid": "loan-amount" }}
                          name="loanAmount"
                          variant="outlined"
                          fullWidth
                          value={values.loanAmount}
                          onChange={(value) => setFieldValue("loanAmount", parseInt(value.target.value))}
                          error={!!(errors.loanAmount && touched.loanAmount)}
                          helperText={errors.loanAmount && touched.loanAmount && errors.loanAmount}
                          FormHelperTextProps={{ classes: errorTextStyles }}
                          InputProps={{
                            startAdornment: <InputAdornment position="start">$</InputAdornment>,
                            inputComponent: CurrencyFormat as any
                          }}
                        />
                      </Grid>
                      <Grid item>
                        <Typography variant="body2" className="field-label">
                          Monthly Installment
                        </Typography>
                        <TextField
                          inputProps={{ "data-testid": "monthly-installment" }}
                          name="monthlyInstallment"
                          variant="outlined"
                          fullWidth
                          value={values.monthlyInstallment}
                          onChange={(value) => setFieldValue("monthlyInstallment", parseInt(value.target.value))}
                          error={!!(errors.monthlyInstallment && touched.monthlyInstallment)}
                          helperText={errors.monthlyInstallment && touched.monthlyInstallment && errors.monthlyInstallment}
                          FormHelperTextProps={{ classes: errorTextStyles }}
                          InputProps={{
                            startAdornment: <InputAdornment position="start">$</InputAdornment>,
                            inputComponent: CurrencyFormat as any
                          }}
                        />
                      </Grid>
                      <Grid item>
                        <Typography variant="body2" className="field-label">
                          First Installment Due
                        </Typography>
                        <KeyboardDatePicker
                          inputProps={{ "data-testid": "first-installment-due" }}
                          id="firstInstallmentDue"
                          inputVariant="outlined"
                          variant="inline"
                          disableToolbar
                          fullWidth
                          format="yyyy/MM/DD"
                          value={values.firstInstallmentDue}
                          error={!!(errors.firstInstallmentDue && touched.firstInstallmentDue)}
                          helperText={
                            errors.firstInstallmentDue && touched.firstInstallmentDue && errors.firstInstallmentDue
                          }
                          onChange={(date) => setFieldValue("firstInstallmentDue", date)}
                          FormHelperTextProps={{ classes: errorTextStyles }}
                        />
                      </Grid>
                    </Grid>

                    <Grid item container direction="column" spacing={5} xs={6}>
                      <Grid item>
                        <Typography variant="body2" className="field-label">
                          Contract Date
                        </Typography>
                        <KeyboardDatePicker
                          inputProps={{ "data-testid": "contract-date" }}
                          id="contractDate"
                          inputVariant="outlined"
                          variant="inline"
                          disableToolbar
                          fullWidth
                          format="yyyy/MM/DD"
                          value={values.contractDate}
                          error={!!(errors.contractDate && touched.contractDate)}
                          helperText={errors.contractDate && touched.contractDate && errors.contractDate}
                          onChange={(date) => setFieldValue("contractDate", date)}
                          FormHelperTextProps={{ classes: errorTextStyles }}
                        />
                      </Grid>
                      <Grid item>
                        <Typography variant="body2" className="field-label">
                          Final Installment (if applicable)
                        </Typography>
                        <TextField
                          inputProps={{ "data-testid": "final-installment" }}
                          name="finalInstallment"
                          variant="outlined"
                          fullWidth
                          value={values.finalInstallment}
                          onChange={(value) => setFieldValue("finalInstallment", parseInt(value.target.value))}
                          error={!!(errors.finalInstallment && touched.finalInstallment)}
                          helperText={errors.finalInstallment && touched.finalInstallment && errors.finalInstallment}
                          FormHelperTextProps={{ classes: errorTextStyles }}
                          InputProps={{
                            startAdornment: <InputAdornment position="start">$</InputAdornment>,
                            inputComponent: CurrencyFormat as any
                          }}
                        />
                      </Grid>
                      <Grid item>
                        <Typography variant="body2" className="field-label">
                          Last Installment Due
                        </Typography>
                        <KeyboardDatePicker
                          inputProps={{ "data-testid": "last-installment-due" }}
                          id="lastInstallmentDue"
                          inputVariant="outlined"
                          variant="inline"
                          disableToolbar
                          fullWidth
                          format="yyyy/MM/DD"
                          value={values.lastInstallmentDue}
                          error={!!(errors.lastInstallmentDue && touched.lastInstallmentDue)}
                          helperText={errors.lastInstallmentDue && touched.lastInstallmentDue && errors.lastInstallmentDue}
                          onChange={(date) => setFieldValue("lastInstallmentDue", date)}
                          FormHelperTextProps={{ classes: errorTextStyles }}
                        />
                      </Grid>
                    </Grid>
                  </Grid>
                </form>
              </MuiPickersUtilsProvider>
            );
          }}
        </Formik>
      </CardContent>
    </Card>
  );
});

export default CreateEditContract;
