const mongoose = require("mongoose");
const fs = require("fs");
const csv = require("csv-parser");
const validator = require("validator");
const config = require("../config/config");
const logger = require("../config/logger");
const { Application, User } = require("../models");

const filePath = process.argv.slice(2)[0];
const mapping = {
  "Email Opt-In": "emailOptIn",
  "Do you have a guarantor?": "guarantor",
  Phone: "phoneNumber"
};

/**
 * returns the camel case version of the word ex: First Name --> firstName
 * @param {string} word word to convert to camel case
 * @returns {string} camel case word
 */
const convertToCamelCase = (word) => {
  const currentWordArray = word.toLowerCase().split(" ");
  let camelCaseWord = currentWordArray[0];
  currentWordArray.slice(1).map((word) => {
    camelCaseWord += word.charAt(0).toUpperCase() + word.slice(1);
  });
  return camelCaseWord;
};

/**
 * maps benefecient header to what we currently store as
 * @param {string} header word to convert to camel case
 * @returns {string} new headername
 */
const mapHeader = (header) => {
  if (header in mapping) {
    return mapping[header];
  } else {
    return convertToCamelCase(header);
  }
};

/**
 * formats the phone number into xxx-xxx-xxxx
 * @param {string} phoneNumber phoneNumber to format
 * @returns {string} formatted phone number
 */
const formatPhoneNumber = (phoneNumber) => {
  //get last 10 digits (ignore +1 etc)
  const number = phoneNumber.replace(/\)|\(|\+|\-|\s/gm, "").substr(-10);
  return number.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
};

/**
 * strips dashes from word and returns camel case format
 * @param {string} word word to strip dashes from
 * @returns {string} formatted word
 */
const stripDashes = (word) => convertToCamelCase(word.replace(/-/g, " "));

/**
 * converts string number/currency to a numeric value
 * @param {string} stringNumber string to convert
 * @returns {number} numeric value if successful, NaN otherwise
 */
const formatNumber = (stringNumber) => {
  const int = parseInt(stringNumber.replace(/[^0-9\.]+/g, ""));
  if (isNaN(int)) {
    return 0;
  } else {
    return int;
  }
};

/**
 * converts date from format DD/MM/YYYY to MM/DD/YYYY
 * @param {string} date date to convert
 * @returns {string} formatted date, and 01/01/0001 otherwise
 */
const formatDate = (date) => {
  //format  of date is DD/MM/YYYY
  const splitDate = date.split("/");

  const formattedDate = new Date(
    parseInt(splitDate[2]),
    parseInt(splitDate[1]) - 1,
    parseInt(splitDate[0])
  ).toLocaleDateString();

  if (formattedDate === "Invalid Date") {
    return "01/01/0001";
  }
  return formattedDate;
};

/**
 * converts string to boolean
 * @param {string} data data to convert
 * @returns {boolean} boolean value, true if yes or 1, false otherwise
 */
const formatBoolean = (data) => {
  if (data === "yes" || data === "1") return true;
  return false;
};

/**
 * formats acknowledgements as defined in the database
 * @param {string} date data to format
 * @returns {Object} object matching the stored version on the database
 */
const formatAcknowledgements = (data) => {
  const loanMapping = {
    "loan-purpose-is-for-credit-card-emergency-or-similar-high-interest-bearing-debt": "loanPurpose",
    "maximum-loan-value-of-usd4-000": "maxLoan",
    "repayment-within-12-months": "repayment",
    "residence-in-canada": "residence",
    "positive-net-income": "netPositiveIncome",
    "guarantor-consent": "guarantorConsent"
  };
  const acknowledgementsObject = {
    loanPurpose: false,
    repayment: false,
    maxLoan: false,
    residence: false,
    netPositiveIncome: false,
    guarantorConsent: false
  };
  const acknowledgements = data.split(",");
  for (const acknowledgement of acknowledgements) {
    if (acknowledgement in loanMapping) {
      acknowledgementsObject[loanMapping[acknowledgement]] = true;
    }
  }
  return acknowledgementsObject;
};

/**
 * santizes data from beneficent to match what will be stored on the database
 * @param {string} key - key of the object to sanitize
 * @param {string} data - date associated with the key to sanitize
 * @returns {string | number} - appropriately santized data
 */
const sanitizeData = (key, data) => {
  const formatMapping = {
    address: "uppercase",
    city: "uppercase",
    province: "uppercase",
    citizenship: "stripDashes",
    phoneNumber: "phoneNumber",
    dateOfBirth: "date",
    dateSubmitted: "date",
    loanAmountRequested: "number",
    employmentStatus: "stripDashes",
    guarantor: "boolean",
    emailOptIn: "boolean",
    acknowledgements: "acknowledgements"
  };
  //trim & get rid of new lines
  data = data.trim().replace(/(\r\n|\n|\r)/gm, "");
  if (!(key in formatMapping)) {
    if (data === "") {
      return "Data not provided.";
    }
    //do not edit first or last name as there is no set rule
    if (key === "firstName" || key === "lastName") {
      return data;
    }
    return data.toLowerCase();
  }
  const format = formatMapping[key];
  switch (format) {
    case "uppercase":
      if (data === "") {
        return "Data not provided.";
      }
      return data.toUpperCase();

    case "date":
      return formatDate(data);

    case "phoneNumber":
      return formatPhoneNumber(data);
    case "number":
      return formatNumber(data);

    case "boolean":
      return formatBoolean(data);

    case "stripDashes":
      return stripDashes(data);

    case "acknowledgements":
      return formatAcknowledgements(data);

    default:
      return data;
  }
};

/**
 * Generates password, this is done soley for script testing, will not be used in prod
 * @param {string} firstName first name of user
 * @param {string} lastName last name of user
 * @param {string} birthday birthday of user
 * @returns temp password generated based on the provided info
 */

const generateUserPassword = (firstName, lastName, birthday) => {
  const date = new Date(birthday);
  return (firstName[0] + date.getFullYear() + lastName[0] + date.getMonth()).repeat(2);
};

/**
 * reads the CSV file and migrates data
 * @param {string} filePath - path to where the file is stored
 * @returns {Promise<Array>} - array of objects with sanitized data
 */
const readCSVFile = (filePath) =>
  new Promise((resolve) => {
    const emailMap = {};
    const pastApplications = [];
    try {
      if (fs.existsSync(filePath)) {
        fs.createReadStream(filePath)
          .pipe(csv({ mapHeaders: ({ header, index }) => mapHeader(header) }))
          .on("data", (data) => {
            if (data.email || data.phoneNumber) {
              //if application doesn't have a phone or email, no way to contact
              const santizedData = {};
              for (const [key, value] of Object.entries(data)) {
                santizedData[key] = sanitizeData(key, value);
              }
              //if not canadian number or invalid email, then we do not add this data
              if (
                !(santizedData.email in emailMap) &&
                validator.isMobilePhone(santizedData.phoneNumber) &&
                validator.isEmail(santizedData.email)
              ) {
                pastApplications.push(santizedData);
                //add email to map to avoid duplicate
                emailMap[santizedData.email] = true;
              }
            }
          })
          .on("finish", () => {
            const listOfApplications = [];
            const listOfUsers = [];
            for (const applicatation of pastApplications) {
              listOfUsers.push(
                new User({
                  firstName: applicatation.firstName,
                  lastName: applicatation.lastName,
                  email: applicatation.email,
                  phoneNumber: applicatation.phoneNumber,
                  address: applicatation.address,
                  country: "Canada",
                  password: generateUserPassword(applicatation.firstName, applicatation.lastName, applicatation.dateOfBirth)
                })
              );
              listOfApplications.push(
                new Application({
                  firstName: applicatation.firstName,
                  lastName: applicatation.lastName,
                  email: applicatation.email,
                  phoneNumber: applicatation.phoneNumber,
                  address: applicatation.address,
                  city: applicatation.city,
                  province: applicatation.province,
                  sex: applicatation.sex,
                  dateOfBirth: applicatation.dateOfBirth,
                  maritalStatus: applicatation.maritalStatus,
                  citizenship: applicatation.citizenship,
                  employmentStatus: applicatation.employmentStatus,
                  loanAmountRequested: applicatation.loanAmountRequested,
                  loanType: applicatation.loanType,
                  debtCircumstances: applicatation.debtCircumstances,
                  preferredLanguage: applicatation.preferredLanguage,
                  emailOptIn: applicatation.emailOptIn,
                  guarantor: applicatation.guarantor,
                  recommendationInfo: "This application was submitted prior to the development of the CRM Application",
                  status: "Received",
                  acknowledgements: applicatation.acknowledgements
                })
              );
            }
            resolve({ applications: listOfApplications, users: listOfUsers });
          });
      } else {
        throw new Error("file does not exist");
      }
    } catch (e) {
      throw new Error(e);
    }
  });

/**
 * gets data from CSV files and adds to database
 * @returns {String} - string if successful
 */
const addDataToDatabase = async () => {
  try {
    const { applications, users } = await readCSVFile(filePath);
    for (let i = 0; i < users.length; i++) {
      const createdUser = await User.create(users[i]);
      applications[i].user = createdUser._id;
    }
    await Application.insertMany(applications);
    logger.info("Migration was successful.");
  } catch (error) {
    logger.error(`Error migrating: ${error}`);
    process.exit(1);
  }
};

mongoose.connect(config.mongoose.url, config.mongoose.options).then((db) => {
  addDataToDatabase();
  db.disconnect();
});
