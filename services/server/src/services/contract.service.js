const httpStatus = require("http-status");
const { Contract, Client } = require("../models");
const ApiError = require("../utils/ApiError");
const createContractPDF = require("../utils/createContractPDF");

/**
 * Create a contract
 * @param {Object} contractBody
 * @returns {Promise<Contract>}
 */
const createContract = async (contractBody) => {
  return Contract.create(contractBody);
};

/**
 * Query for contracts
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.populate] - Populate data fields. Hierarchy of fields should be separated by (.). Multiple populating criteria should be separated by commas (,)
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryContracts = async (filter, options) => {
  let contractFilter = { client: { $in: [] } };
  let validClientIds = [];

  if (filter.application) {
    const clients = await Client.find({ application: filter.application });
    clients.forEach((client) => validClientIds.push(client._id));
  }

  if (filter.client) validClientIds.push(filter.client);

  if (filter.officer) {
    const clients = await Client.find({ officer: filter.officer });
    clients.forEach((client) => validClientIds.push(client._id));
  }

  validClientIds.length > 0 ? (contractFilter.client.$in = validClientIds) : delete contractFilter.client;

  const contracts = await Contract.paginate(contractFilter, options);

  if (contracts.results.length === 0) {
    throw new ApiError(httpStatus.NOT_FOUND, "No contracts found");
  }
  return contracts;
};

/**
 * Get contract by id
 * @param {ObjectId} contractId
 * @returns {Promise<Contract>}
 */
const getContractById = async (contractId) => {
  return Contract.findById(contractId);
};

/**
 * Update contract by id
 * @param {ObjectId} contractId
 * @param {Object} updateBody
 * @returns {Promise<Contract>}
 */
const updateContractById = async (contractId, updateBody) => {
  const contract = await getContractById(contractId);
  if (!contract) {
    throw new ApiError(httpStatus.NOT_FOUND, "Contract not found");
  }

  const approvedAmount = updateBody.approvedLoanAmount ? updateBody.approvedLoanAmount : contract.approvedLoanAmount;

  const finalPaymentTime = updateBody.finalPaymentDue
    ? updateBody.finalPaymentDue.getTime()
    : contract.finalPaymentDue.getTime();

  const firstPaymentTime = updateBody.firstPaymentDue
    ? updateBody.firstPaymentDue.getTime()
    : contract.firstPaymentDue.getTime();

  if (!updateBody.monthlyPayment) {
    updateBody.monthlyPayment = calculateNewMonthlyPayment(approvedAmount, finalPaymentTime, firstPaymentTime);
  }

  if (!updateBody.finalPayment) {
    updateBody.finalPayment = calculateNewFinalPayment(
      approvedAmount,
      updateBody.monthlyPayment,
      finalPaymentTime,
      firstPaymentTime
    );
  }

  Object.assign(contract, updateBody);
  await contract.save();
  return contract;
};

const secondsInMonth = 2628000000;

/**
 * @param {Number} approvedAmount
 * @param {Number} finalPaymentTime - Unix time
 * @param {Number} firstPaymentTime - Unix time
 * @returns - Updated monthly payment rounded to the nearest 5
 */
const calculateNewMonthlyPayment = (approvedAmount, finalPaymentTime, firstPaymentTime) => {
  // approvedAmount, finalPaymentTime and firstPaymentTime are chosen based on whether or not
  // They were provided in the updateBody, otherwise, they are taken from the contract

  // Approved amount divided by number of months (rounded to the nearest month), rounded to nearest 5
  return Math.ceil(approvedAmount / Math.round((finalPaymentTime - firstPaymentTime) / secondsInMonth) / 5) * 5;
};

/**
 * @param {Number} approvedAmount
 * @param {Number} monthlyPayment
 * @param {Number} finalPaymentTime - Unix time
 * @param {Number} firstPaymentTime - Unix time
 * @returns - Updated final payment after (# of months in term - 1) monthly payments have been made
 */
const calculateNewFinalPayment = (approvedAmount, monthlyPayment, finalPaymentTime, firstPaymentTime) => {
  // approvedAmount, finalPaymentTime and firstPaymentTime are chosen based on whether or not
  // They were provided in the updateBody, otherwise, they are taken from the contract
  // monthlyPayment will always either be sent with the updateBody or calculated

  // The remaining amount to pay after all but one month has been payed off at the monthly payment rate
  // ((finalPaymentTime - firstPaymentTime - secondsInMonth) / secondsInMonth) = # of months in payment term - 1 month

  const finalPayment =
    approvedAmount - monthlyPayment * Math.round((finalPaymentTime - firstPaymentTime - secondsInMonth) / secondsInMonth);

  // There may be cases where the new final payment calculated results in the loan being paid off earlier than required. In that case, return the final
  // amount that would be paid without exceeding the loan
  return finalPayment >= 0 ? finalPayment : monthlyPayment + finalPayment;
};

/**
 * Delete contract by id
 * @param {ObjectId} contractId
 * @returns {Promise<Contract>}
 */
const deleteContractById = async (contractId) => {
  const contract = await getContractById(contractId);
  if (!contract) {
    throw new ApiError(httpStatus.NOT_FOUND, "Contract not found");
  }
  await contract.remove();
  return contract;
};

/**
 * Create contract PDF by id
 * @param {ObjectId} contractId
 * @returns {String} - Name of contract PDF file
 */
const createContractPDFById = async (contractId) => {
  const contract = await getContractById(contractId);
  if (!contract) {
    throw new ApiError(httpStatus.NOT_FOUND, "Contract not found");
  }

  await contract
    .populate({
      path: "client",
      populate: {
        path: "application officer",
        populate: {
          path: "user"
        }
      }
    })
    .execPopulate();
  const fileBuffer = createContractPDF(contract);
  contract.contractFileName = `Loan_Contract-${contract._id}.pdf`;
  await contract.save();
  return fileBuffer;
};

/**
 * Upload contract PDF
 * @param {ObjectId} contractId
 * @param {Buffer} contractFileBuffer
 * @returns {Promise<Contract>}
 */
const uploadContract = async (contractId, contractFileBuffer) => {
  const contract = await getContractById(contractId);
  if (!contract) {
    throw new ApiError(httpStatus.NOT_FOUND, "Contract not found");
  }
  contract.contractFileName = `Signed_Loan_Contract-${contract._id}.pdf`;
  contract.contractFileBuffer = contractFileBuffer;
  await contract.save();
  return contract;
};

module.exports = {
  createContract,
  queryContracts,
  getContractById,
  updateContractById,
  deleteContractById,
  createContractPDFById,
  uploadContract
};
