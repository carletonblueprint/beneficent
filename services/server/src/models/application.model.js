const mongoose = require("mongoose");
const validator = require("validator");
const { toJSON, paginate } = require("./plugins");

const applicationSchema = mongoose.Schema(
  {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error("Invalid email");
        }
      }
    },
    phoneNumber: {
      type: String,
      required: true,
      validate(value) {
        if (!validator.isMobilePhone(value)) {
          throw new Error("Invalid phone number");
        }
      }
    },
    address: { type: String, required: true },
    city: { type: String, required: true },
    province: { type: String, required: true },
    sex: { type: String, required: true },
    dateOfBirth: { type: Date, required: true },
    maritalStatus: { type: String, required: true },
    citizenship: { type: String, required: true },
    preferredLanguage: { type: String, required: true },
    employmentStatus: { type: String, required: true },
    loanAmountRequested: { type: Number, required: true },
    loanType: { type: String, required: true },
    debtCircumstances: { type: String, required: true },
    guarantor: {
      hasGuarantor: Boolean,
      fullName: String,
      email: {
        type: String,
        unique: true,
        trim: true,
        lowercase: true,
        validate(value) {
          if (!validator.isEmail(value)) {
            throw new Error("Invalid email");
          }
        }
      },
      phoneNumber: {
        type: String,
        validate(value) {
          if (!validator.isMobilePhone(value)) {
            throw new Error("Invalid phone number");
          }
        }
      }
    },
    recommendationInfo: { type: String, required: true },
    acknowledgements: {
      loanPurpose: { type: Boolean, required: true },
      maxLoan: { type: Boolean, required: true },
      repayment: { type: Boolean, required: true },
      residence: { type: Boolean, required: true },
      netPositiveIncome: { type: Boolean, required: true },
      guarantorConsent: { type: Boolean, required: true }
    },
    emailOptIn: { type: Boolean, required: true },
    status: { type: String, required: true },
    officer: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Officer",
      required: false
    },
    user: { type: mongoose.SchemaTypes.ObjectId, ref: "User", required: false }
  },
  {
    timestamps: true
  }
);

applicationSchema.plugin(toJSON);
applicationSchema.plugin(paginate);

/**
 * Check if email is taken
 * @param {string} email - The email
 * @returns {Promise<boolean>}
 */
applicationSchema.statics.isDuplicate = async function (email) {
  const application = await this.findOne({ email });
  return !!application;
};

/**
 * Update workload for officers
 */
applicationSchema.post("save", async function () {
  await mongoose.model("Officer").updateWorkload();
});

/**
 * Update workload for officers
 */
applicationSchema.post("remove", async function () {
  await mongoose.model("Officer").updateWorkload();
});

/**
 * @typedef Application
 */
const Application = mongoose.model("Application", applicationSchema);

module.exports = Application;
