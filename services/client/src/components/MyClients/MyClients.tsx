import React from "react";
import { Breadcrumbs, Paper, withStyles, Typography } from "@material-ui/core";
import "./MyClients.scss";

/**
 * A styled typography component for the titles
 */
export const TitleTypography = withStyles({
  h4: {
    paddingLeft: 15,
    fontWeight: 700
  },
  h6: {
    paddingLeft: 15,
    fontWeight: 700
  }
})(Typography);

const MyClients = () => {
  return (
    <React.Fragment>
      <Breadcrumbs>
        <TitleTypography variant="h4" color="textPrimary" className="main" style={{ marginBottom: 10 }}>
          My Clients
        </TitleTypography>
      </Breadcrumbs>
      <Paper className="main-content" elevation={0} square></Paper>
    </React.Fragment>
  );
};

export default MyClients;
