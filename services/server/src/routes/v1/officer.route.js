const express = require("express");
const auth = require("../../middlewares/auth");
const validate = require("../../middlewares/validate");
const { officerValidation } = require("../../validations");
const { officerController } = require("../../controllers");

const router = express.Router();

router
  .route("/")
  .post(auth("manageOfficers"), validate(officerValidation.createOfficer), officerController.createOfficer)
  .get(auth("getOfficers"), validate(officerValidation.getOfficers), officerController.getOfficers);

router
  .route("/:officerId")
  .get(auth("getOfficers"), validate(officerValidation.getOfficer), officerController.getOfficer)
  .patch(auth("manageOfficers"), validate(officerValidation.updateOfficer), officerController.updateOfficer)
  .delete(auth("manageOfficers"), validate(officerValidation.deleteOfficer), officerController.deleteOfficer);

module.exports = router;

/**
 * @swagger
 * tags:
 *   name: Officers
 *   description: Officer management and retrieval
 */

/**
 * @swagger
 *  /officers:
 *    post:
 *      summary: Create a officer
 *      description: Only admins can create other officers.
 *      tags: [Officers]
 *      security:
 *        - bearerAuth: []
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              required:
 *                - user
 *              properties:
 *                user:
 *                  type: string
 *                  $ref: '#/components/schemas/User'
 *              example:
 *                user: 6011e5410077e70030610e8e
 *      responses:
 *        "201":
 *          description: Created
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/Officer'
 *        "400":
 *          $ref: '#/components/responses/DuplicateOfficer'
 *
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *
 *    get:
 *      summary: Get all officers
 *      description: Only admins can retrieve all officers.
 *      tags: [Officers]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: query
 *          name: firstName
 *          schema:
 *            type: string
 *          description: User first name
 *        - in: query
 *          name: lastName
 *          schema:
 *            type: string
 *          description: User last name
 *        - in: query
 *          name: email
 *          schema:
 *            type: string
 *            format: email
 *          description: User email
 *        - in: query
 *          name: user
 *          schema:
 *            type: string
 *          description: User ID
 *        - in: query
 *          name: populate
 *          schema:
 *            type: string
 *          description: Populate data fields. Hierarchy of fields should be separated by (.). Multiple populating criteria should be separated by commas (,) (ex. user)
 *        - in: query
 *          name: sortBy
 *          schema:
 *            type: string
 *          description: Sort by query in the form of field:desc/asc (ex. name:asc)
 *        - in: query
 *          name: limit
 *          schema:
 *            type: integer
 *            minimum: 1
 *          default: 10
 *          description: Maximum number of officers
 *        - in: query
 *          name: page
 *          schema:
 *            type: integer
 *            minimum: 1
 *            default: 1
 *          description: Page number
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  results:
 *                    type: array
 *                    items:
 *                      $ref: '#/components/schemas/Officer'
 *                  page:
 *                    type: integer
 *                    example: 1
 *                  limit:
 *                    type: integer
 *                    example: 10
 *                  totalPages:
 *                    type: integer
 *                    example: 1
 *                  totalResults:
 *                    type: integer
 *                    example: 1
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */

/**
 * @swagger
 *  /officers/{id}:
 *    get:
 *      summary: Get a officer
 *      description: Only admins can fetch officers.
 *      tags: [Officers]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Officer id
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/Officer'
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 *
 *    patch:
 *      summary: Update a officer
 *      description: Only admins can update officers.
 *      tags: [Officers]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Officer id
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                user:
 *                  type: string
 *                  description: User id
 *              example:
 *                user: 4abc40c86762e0fb12000123
 *      responses:
 *        "200":
 *          description: OK
 *          content:
 *            application/json:
 *              schema:
 *                 $ref: '#/components/schemas/Officer'
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 *
 *    delete:
 *      summary: Delete a officer
 *      description: Only admins can delete officers.
 *      tags: [Officers]
 *      security:
 *        - bearerAuth: []
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: Officer id
 *      responses:
 *        "200":
 *          description: No content
 *        "401":
 *          $ref: '#/components/responses/Unauthorized'
 *        "403":
 *          $ref: '#/components/responses/Forbidden'
 *        "404":
 *          $ref: '#/components/responses/NotFound'
 */
