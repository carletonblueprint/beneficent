import { ApiCore } from "../utilities/core";

export const apiOfficers = new ApiCore({
  getAll: true,
  getSingle: true,
  post: true,
  patch: true,
  delete: true,
  upload: false,
  download: false,
  url: "officers"
});
