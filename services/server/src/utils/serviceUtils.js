const ApiError = require("./ApiError");
const { User, Application, Officer } = require("../models");
const httpStatus = require("http-status");

/**
 * Validates id from user collection
 * @param {ObjectId} userId - The user id
 * @param {string} errorMessage - The relevant error message
 * @throws {ApiError} - if the ID does not exist in the mongoDB database
 * @returns void
 */
const validateUserId = async (userId, errorMessage) => {
  const user = await User.findById(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, errorMessage);
  }
};

/**
 * Validates id from officer collection
 * @param {ObjectId} officerId - The officer id
 * @param {string} errorMessage - The relevant error message
 * @throws {ApiError} - if the ID does not exist in the mongoDB database
 * @returns void
 */
const validateOfficerId = async (officerId, errorMessage) => {
  const user = await Officer.findById(officerId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, errorMessage);
  }
};

/**
 * Validates id from application collection
 * @param {ObjectId} applicationId - The application id
 * @param {string} errorMessage - The relevant error message
 * @throws {ApiError} - if the ID does not exist in the mongoDB database
 * @returns void
 */
const validateApplicationId = async (applicationId, errorMessage) => {
  const app = await Application.findById(applicationId);
  if (!app) {
    throw new ApiError(httpStatus.NOT_FOUND, errorMessage);
  }
};

/**
 * Validates acknowledgments attribute from application
 * @param {Object} acknowledgements - the acknowledgments attribute
 * @param {string} errorMessage - The relevant error message
 * @throws {ApiError} - if not all acknowledgments are met
 */
const validateAcknowledgements = async (acknowledgements, errorMessage) => {
  Object.keys(acknowledgements).forEach((key) => {
    if (!acknowledgements[key]) {
      throw new ApiError(httpStatus.BAD_REQUEST, errorMessage);
    }
  });
};

module.exports = {
  validateUserId,
  validateOfficerId,
  validateApplicationId,
  validateAcknowledgements
};
