const mongoose = require("mongoose");
const setupTestDB = require("../../utils/setupTestDB");
const { Document } = require("../../../src/models");

setupTestDB();

describe("Document model", () => {
  describe("Document validation", () => {
    let newDocument;
    beforeEach(() => {
      newDocument = {
        originalName: "file.pdf",
        buffer: Buffer.from("test"),
        mimeType: "application/pdf",
        path: "src/files/file.pdf",
        size: 123456,
        docType: "Other",
        application: mongoose.Types.ObjectId()
      };
    });

    test("should correctly validate a valid document", async () => {
      await expect(new Document(newDocument).validate()).resolves.toBeUndefined();
    });

    test("should correctly throw error on an invalid application", async () => {
      newDocument.application = "invalidApplication";
      await expect(new Document(newDocument).validate()).rejects.toThrow();
    });
  });
});
