export interface getUsers {
  firstName?: string;
  lastName?: string;
  phoneNumber?: string;
  address?: string;
  country?: string;
  email?: string;
  role?: string;
  populate?: string;
  sortBy?: string;
  limit?: number;
  page?: number;
}

export interface createUser {
  firstName: string;
  lastName: string;
  phoneNumber: string;
  address: string;
  country: string;
  email: string;
  password: string;
  role: string;
}

export interface updateUser {
  firstName?: string;
  lastName?: string;
  phoneNumber?: string;
  address?: string;
  country?: string;
  email?: string;
  role?: string;
}
