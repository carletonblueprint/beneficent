import { ApiCore } from "../utilities/core";

export const apiContracts = new ApiCore({
  getAll: true,
  getSingle: true,
  post: true,
  patch: true,
  delete: true,
  upload: true,
  download: true,
  generateHeaders: true,
  url: "contracts"
});
