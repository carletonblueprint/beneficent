import { cleanup, render } from "@testing-library/react";
import * as React from "react";
import ApplicationDetails from "../components/ApplicationsLayout/ApplicationDetails/ApplicationDetails";
import { MemoryRouter } from "react-router-dom";
import routes from "../routes";
import { FormikSubmitObject } from "../components/ApplicationsLayout/ApplicationsLayout";

afterEach(cleanup);

const mockRef = React.createRef<FormikSubmitObject>();

describe("Application Details", () => {
  it("matches snapshot", () => {
    const { asFragment } = render(
      <MemoryRouter initialEntries={[routes.casesApplications.path + "/:id"]}>
        <ApplicationDetails createEditContractSubmissionRef={mockRef} />
      </MemoryRouter>
    );

    expect(asFragment()).toMatchSnapshot();
  });

  it("renders correctly", () => {
    const { queryByTestId } = render(
      <MemoryRouter initialEntries={[routes.casesApplications.path + "/:id"]}>
        <ApplicationDetails createEditContractSubmissionRef={mockRef} />
      </MemoryRouter>
    );

    expect(queryByTestId("skeleton-container")).toHaveClass("MuiGrid-container MuiGrid-spacing-xs-3");
  });
});
