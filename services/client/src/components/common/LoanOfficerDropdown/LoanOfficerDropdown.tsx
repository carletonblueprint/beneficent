import { FormControl, InputBase, MenuItem, Select, withStyles } from "@material-ui/core";
import "./LoanOfficerDropdown.scss";
import { ExpandMore } from "@material-ui/icons";
import React from "react";

/**
 * Props for loan officer dropdown
 */
type LoanOfficerDropdownProps = {
  loanOfficers: { name: ""; avatar: ""; status: "" }[];
  value: string;
};

/**
 * Customized Drop Down Input for AssignedTo and Status fields
 */
const AssignedToInput = withStyles((theme) => ({
  input: {
    width: 127,
    height: 16,
    border: "1px solid rgb(0, 0, 0, 0.12)",
    borderRadius: 4,
    fontSize: 16,
    color: "black",
    paddingLeft: 11,
    paddingBottom: 10
  }
}))(InputBase);

/**
 * Renders Loan Officer dropdown to assign a loan officer to an
 *  application
 */
const LoanOfficerDropdown: React.FC<LoanOfficerDropdownProps> = (props) => {
  const { loanOfficers, value } = props;
  // Set default to be existing assignedTo value in database
  const [assignedTo, setAssignedTo] = React.useState(value);

  const handleChangeAssignedTo = (event) => {
    setAssignedTo(event.target.value);
    // TODO: update on backend database
  };

  return (
    <FormControl variant="outlined">
      <Select
        labelId="assignedTo-select-label"
        id="assignedTo-select"
        value={assignedTo}
        onChange={handleChangeAssignedTo}
        input={<AssignedToInput />}
        inputProps={{
          classes: {
            icon: "dropdown-icon"
          }
        }}
        displayEmpty
        renderValue={assignedTo !== "" ? undefined : () => <span className="empty">Not Assigned</span>}
        IconComponent={ExpandMore}
      >
        {loanOfficers.map((loanOfficer) => (
          <MenuItem value={loanOfficer.name} key={loanOfficer.name}>
            <div className="profile">
              <div className="status-bar"></div>
              <img src={loanOfficer.avatar} />
              <span>{loanOfficer.name}</span>
            </div>
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default LoanOfficerDropdown;
