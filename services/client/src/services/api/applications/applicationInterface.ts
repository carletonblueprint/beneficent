export interface createApplication {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  address: string;
  city: string;
  province: string;
  sex: string;
  dateOfBirth: string;
  maritalStatus: string;
  citizenship: string;
  preferredLanguage: string;
  employmentStatus: string;
  loanAmountRequested: number;
  loanType: string;
  debtCircumstances: string;
  guarantor?: {
    fullName: string;
    email: string;
    phoneNumber: string;
  };
  recommendationInfo: string;
  acknowledgements: {
    loanPurpose: boolean;
    maxLoan: boolean;
    repayment: boolean;
    residence: boolean;
    netPositiveIncome: boolean;
    guarantorConsent: boolean;
  };
  emailOptIn: boolean;
  status: string;
  officer?: string;
  user?: string;
}

export interface getApplications {
  nameOrEmail?: string;
  status?: string[] | string;
  displayAll?: boolean;
  officer?: string[];
  user?: string;
  populate?: string;
  sortBy?: string;
  limit?: number;
  page?: number;
}

export interface updateApplication {
  firstName?: string;
  lastName?: string;
  email?: string;
  phoneNumber?: string;
  address?: string;
  city?: string;
  province?: string;
  sex?: string;
  dateOfBirth?: string;
  maritalStatus?: string;
  citizenship?: string;
  preferredLanguage?: string;
  employmentStatus?: string;
  loanAmountRequested?: number;
  loanType?: string;
  debtCircumstances?: string;
  guarantor?: {
    fullName: string;
    email: string;
    phoneNumber: string;
  };
  recommendationInfo?: string;
  emailOptIn?: boolean;
  status?: string;
  officer?: string;
  user?: string;
}
