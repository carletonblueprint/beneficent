const mongoose = require("mongoose");
const { toJSON, paginate } = require("./plugins");

const secondsInMonth = 2628000000;

/**
 * @param {Number} approvedAmount
 * @param {Number} finalPaymentTime - Unix time
 * @param {Number} firstPaymentTime - Unix time
 * @returns {Number} - Monthly payment rounded to the nearest 5
 */
const calculateMonthlyPayment = (approvedAmount, finalPaymentTime, firstPaymentTime) => {
  // Approved amount divided by number of months (rounded to the nearest month), rounded to nearest 5
  return Math.ceil(approvedAmount / Math.round((finalPaymentTime - firstPaymentTime) / secondsInMonth) / 5) * 5;
};

/**
 * @param {Number} approvedAmount
 * @param {Number} monthlyPayment
 * @param {Number} finalPaymentTime - Unix time
 * @param {Number} firstPaymentTime - Unix time
 * @returns {Number} - Final payment after (# of months in term - 1) monthly payments have been made
 */
const calculateFinalPayment = (approvedAmount, monthlyPayment, finalPaymentTime, firstPaymentTime) => {
  // The remaining amount to pay after all but one month has been payed off at the monthly payment rate
  // ((finalPaymentTime - firstPaymentTime - secondsInMonth) / secondsInMonth) = # of months in payment term - 1 month
  return (
    approvedAmount - monthlyPayment * Math.round((finalPaymentTime - firstPaymentTime - secondsInMonth) / secondsInMonth)
  );
};

const contractSchema = mongoose.Schema(
  {
    guarantorName: { type: String, required: true },
    approvedLoanAmount: { type: Number, required: true },
    firstPaymentDue: { type: Date, required: true },
    finalPaymentDue: { type: Date, required: true },
    monthlyPayment: {
      type: Number,
      default: function () {
        return calculateMonthlyPayment(
          this.approvedLoanAmount,
          this.finalPaymentDue.getTime(),
          this.firstPaymentDue.getTime()
        );
      }
    },
    finalPayment: {
      type: Number,
      default: function () {
        return calculateFinalPayment(
          this.approvedLoanAmount,
          this.monthlyPayment,
          this.finalPaymentDue.getTime(),
          this.firstPaymentDue.getTime()
        );
      }
    },
    status: { type: String, required: true },
    contractFileName: { type: String, default: "" },
    contractFileBuffer: { type: Buffer, private: true },
    contractStartDate: { type: Date, required: true },
    client: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Client",
      required: true
    }
  },
  {
    timestamps: true
  }
);

contractSchema.plugin(toJSON);
contractSchema.plugin(paginate);

/**
 * @typedef Contract
 */
const Contract = mongoose.model("Contract", contractSchema);

module.exports = Contract;
