export interface createClient {
  dateSigned: string;
  officer: string;
  application: string;
}

export interface getClients {
  nameOrEmail?: string;
  officer?: string[];
  startDate?: string;
  endDate?: string;
  populate?: string;
  sortBy?: string;
  limit?: number;
  page?: number;
}

export interface updateClient {
  officer: string;
}
