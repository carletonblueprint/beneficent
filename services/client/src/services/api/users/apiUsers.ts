import { ApiCore } from "../utilities/core";

export const apiUsers = new ApiCore({
  getAll: true,
  getSingle: true,
  post: true,
  patch: true,
  delete: true,
  upload: false,
  download: false,
  generateHeaders: true,
  url: "users"
});
