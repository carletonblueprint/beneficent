import { ApiCore } from "../utilities/core";

export const apiApplications = new ApiCore({
  getAll: true,
  getSingle: true,
  post: true,
  patch: true,
  delete: true,
  generateHeaders: true,
  upload: false,
  download: false,
  url: "applications"
});
