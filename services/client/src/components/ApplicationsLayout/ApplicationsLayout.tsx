import {
  Button,
  createMuiTheme,
  Divider,
  Chip,
  Grid,
  ThemeProvider,
  withStyles,
  FormControl,
  Select,
  MenuItem,
  InputBase,
  Paper
} from "@material-ui/core";
import ArrowBack from "@material-ui/icons/ArrowBack";
import CancelIcon from "@material-ui/icons/Cancel";
import * as React from "react";
import { Link as RouterLink, Route, RouteComponentProps, Switch, useRouteMatch, withRouter } from "react-router-dom";
import routes from "../../routes";
import { TitleTypography } from "../HomePage/AdminHomePage/AdminHomePage";
import "./ApplicationsLayout.scss";
import { Add, ExpandLess, ExpandMore } from "@material-ui/icons";
import ApplicationsFilters from "./ApplicationsFilters/ApplicationsFilters";
import { MaterialUiPickersDate } from "@material-ui/pickers/typings/date";
import MyTable, { ColumnType } from "../common/Table/Table";
import LoanOfficerDropdown from "../common/LoanOfficerDropdown/LoanOfficerDropdown";
import axios, { AxiosRequestConfig } from "axios";
import moment from "moment";
import ApplicationDetails from "./ApplicationDetails/ApplicationDetails";
import Dialogue from "../common/Dialogue/Dialogue";

type Application = {
  guarantor: {
    hasGuarantor: boolean;
    fullName: string;
    email: string;
    phoneNumber: string;
  };
  acknowledgements: {
    loanPurpose: boolean;
    maxLoan: boolean;
    residence: boolean;
    netPositiveIncome: boolean;
    guarantorConsent: boolean;
    repayment: boolean;
  };
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  address: string;
  city: string;
  province: string;
  sex: string;
  dateOfBirth: string;
  maritalStatus: string;
  citizenship: string;
  preferredLanguage: string;
  employmentStatus: string;
  loanAmountRequested: number;
  loanType: string;
  debtCircumstances: string;
  recommendationInfo: string;
  emailOptIn: boolean;
  status: string;
  officer: string;
  user: string;
  createdAt: string;
  updatedAt: string;
  id: string;
};

export interface FormikSubmitObject {
  handleSubmit: () => void;
}

/**
 * Map path to breadcrumb name
 */
export const breadcrumbMap = {
  [routes.casesApplications.path]: "Cases & Applications",
  [routes.create.path]: "Create",
  [routes.detail.path]: "Details"
};

/**
 * Indicates all of filter sections
 */
export enum FilterSections {
  STATUS = "Status",
  ASSIGNED_TO = "Assigned To",
  BEFORE = "Before",
  AFTER = "After",
  SORT_BY = "Sort By"
}

// TODO Please make change to these if it's not correct
export type LoanOfficerType = {
  name: string;
  avatar?: string; // The url to the avatar
  busyLevel: 1 | 2 | 3; // TODO change this if needed: 1, 2, 3 corresponding to green, yellow, red
  selected: boolean;
};

/**
 * Type of selection in Status section
 */
export type FilterCheckboxSelectionType = {
  title: string;
  checked: boolean;
};

/**
 * Type of one-per-time filter section
 */
export type FilterChooseSelectionType = {
  selected: number;
  selections: Array<string>;
};

type FilterChipType = {
  type: FilterSections;
  title: string;
};

interface BackButtonProps {
  /**
   * Array of path names of current path
   */
  pathnames: Array<string>;
}

/**
 * Customized Next Nav icon for breadcrumbs
 */

const BackButton = ({ pathnames }: BackButtonProps) => {
  if (pathnames[pathnames.length - 2] === "/create-contract" || pathnames[pathnames.length - 2] === "/edit-contract") {
    // to account for last /:contractId in the route
    pathnames.length -= 2;
  } else {
    pathnames.length -= 1;
  }
  let url = pathnames.join("");

  return (
    <RouterLink className="back-parent" to={"" + url}>
      <TitleTypography className="back-button" variant="h6" color="textPrimary">
        <ArrowBack style={{ marginRight: "12px" }} />
        Back
      </TitleTypography>
    </RouterLink>
  );
};

/**
 * This is used to show the small vertical line that represents
 * the busyness of a Loan Officer
 */
export const BusyLevelIndicator: React.FC<{ level: 1 | 2 | 3 }> = ({ level }) => {
  let color: string;
  switch (level) {
    case 1:
      color = "#70D19C";
      break;
    case 2:
      color = "#FFB946";
      break;
    case 3:
      color = "#F7685B";
      break;
    default:
      color = "#70D19C";
      break;
  }

  return (
    <div
      className="busy-level-indicator"
      style={{
        backgroundColor: color
      }}
    />
  );
};

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#DA6E5D",
      contrastText: "#000"
    }
  },
  typography: {
    allVariants: {
      fontFamily: "Lato"
    }
  }
});

/**
 * Customized Drop Down Input for Status field
 */
const StatusInput = withStyles(() => ({
  input: {
    width: 127,
    height: 16,
    border: "1px solid rgb(0, 0, 0, 0.12)",
    borderRadius: 4,
    fontSize: 16,
    color: "black",
    paddingTop: 6,
    paddingLeft: 22,
    paddingBottom: 10
  }
}))(InputBase);

/**
 * Renders Application Status dropdown to update the status of an
 *  application
 */
function StatusDropdown(props) {
  const { officer } = props;
  const [status, setStatus] = React.useState(props.value);
  const [warning, setWarning] = React.useState(officer == undefined ? true : false);

  const handleChangeStatus = (event) => {
    if (officer == undefined) {
      setWarning(true);
      setStatus(status);
    } else {
      setStatus(event.target.value);
    }
    // TODO: update on backend database
  };

  const applicationStatus = [
    "Received",
    "Interview",
    "Accepted",
    "Rejected",
    "Contract Sent",
    "Contract Signed",
    "Active Client",
    "Archived",
    "Contacted",
    "Requested Information"
  ];
  return (
    <FormControl variant="outlined">
      <Select
        labelId="status-select-label"
        id="status-select"
        value={status}
        onChange={handleChangeStatus}
        input={<StatusInput />}
        inputProps={{
          classes: {
            icon: "dropdown-icon"
          }
        }}
        IconComponent={ExpandMore}
      >
        {warning ? (
          <Dialogue
            title={"Warning"}
            text={"You cannot change this status until a loan officer is assigned. Please assign a loan officer."}
            open={warning}
          />
        ) : null}
        <div className="status-dropdown">
          <p>Status</p>
          <ExpandLess className="icon" />
        </div>
        <Divider />
        {applicationStatus.map((appStatus) => (
          <MenuItem value={appStatus} key={appStatus}>
            <p className="status-menu-item">{appStatus}</p>
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}

/**
 * Method to retrieve all applications from database
 */
function getApplications(accessToken, page, limit, sortBy: string | undefined = undefined): Promise<any> {
  const options: AxiosRequestConfig = {
    url: `${process.env.REACT_APP_API_ENDPOINT}/api/v1/applications?limit=${limit}&page=${page}${
      sortBy ? `&sortBy=${sortBy}` : ""
    }`,
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${accessToken}`
    }
  };

  return axios(options).then((res) => res.data);
}

/**
 * HOC Layout Component for cases and applications
 * @param props RouteComponentProps given by withRouter
 */
const ApplicationsLayout: React.FC<RouteComponentProps> = ({ location }) => {
  const pathnames = location.pathname.split(/(?=\/)/g);
  const { path } = useRouteMatch();
  const accessToken = localStorage.getItem("accessToken");
  const createContractSubmissionRef = React.useRef<FormikSubmitObject>(null);
  // TODO Make this an array of selected chips, so when we select one,
  // add a chip with that information into this array
  const [selectedFilters, setSelectedFilters] = React.useState<Array<FilterChipType>>([]);

  // TODO This state will be used to filter the list
  const [statusSelections, setStatusSelections] = React.useState<Array<FilterCheckboxSelectionType>>([
    { title: "Received", checked: false },
    { title: "Interview", checked: false },
    { title: "Accepted", checked: false },
    { title: "Rejected", checked: false },
    { title: "Contract Sent", checked: false },
    { title: "Contract Signed", checked: false },
    { title: "Active Client", checked: false },
    { title: "Archived", checked: false }
  ]);

  // TODO This state will be used to filter the list
  const [sortBySelections, setSortBySelections] = React.useState<FilterChooseSelectionType>({
    selected: -1,
    selections: ["A-Z", "Z-A", "Latest", "Newest"]
  });

  // TODO This state will be used to filter the list
  // TODO This will be changed based on the database (call API for list of officers)
  // Maybe we can add ID to each Loan Officer so that we can avoid loan officer with the same name when we filter
  const [assignedToSelections, setAssignedToSelections] = React.useState<Array<LoanOfficerType>>([
    { name: "Tasha", busyLevel: 1, selected: false },
    { name: "Denis", busyLevel: 3, selected: false },
    { name: "Alfie", busyLevel: 2, selected: false },
    { name: "Dexter", busyLevel: 1, selected: false }
  ]);

  const [startDate, setStartDate] = React.useState<MaterialUiPickersDate | null>(null);
  const [endDate, setEndDate] = React.useState<MaterialUiPickersDate | null>(null);

  const handleRemoveChip = (title: string) => {
    setSelectedFilters(selectedFilters.filter((filter) => filter.title !== title));
  };

  const handleAddChip = (title: string, type: FilterSections) => {
    setSelectedFilters([...selectedFilters, { type, title }]);
  };

  const handleRemoveSortChip = () => {
    setSelectedFilters(selectedFilters.filter((filter) => filter.type !== FilterSections.SORT_BY));
    setSortBySelections({ ...sortBySelections, selected: -1 });
  };

  const handleReplaceChipTitle = (oldTitle: string, newTitle: string) => {
    setSelectedFilters(
      selectedFilters.map((filter) => (filter.title !== oldTitle ? filter : { ...filter, title: newTitle }))
    );
  };

  /**
   * Handle input of start date
   * @param date value of Date from input
   */
  const handleStartDate = (date: MaterialUiPickersDate) => {
    setStartDate(date);
    let newFilters = selectedFilters.filter((filter) => filter.type !== FilterSections.AFTER);
    if (date) newFilters.push({ type: FilterSections.AFTER, title: date.format("MM/DD/yyyy") });
    setSelectedFilters(newFilters);
  };

  /**
   * Handle input of end date
   * @param date value of Date from input
   */
  const handleEndDate = (date: MaterialUiPickersDate) => {
    setEndDate(date);
    let newFilters = selectedFilters.filter((filter) => filter.type !== FilterSections.BEFORE);
    if (date) newFilters.push({ type: FilterSections.BEFORE, title: date.format("MM/DD/yyyy") });
    setSelectedFilters(newFilters);
  };

  /**
   * Handle apply button click
   */
  const handleApplyDate = () => {
    let newFilters = selectedFilters.filter(
      (filter) => filter.type !== FilterSections.BEFORE && filter.type !== FilterSections.AFTER
    );
    if (startDate) newFilters.push({ type: FilterSections.AFTER, title: startDate.format("MM/DD/yyyy") });
    if (endDate) newFilters.push({ type: FilterSections.BEFORE, title: endDate.format("MM/DD/yyyy") });
    setSelectedFilters(newFilters);
  };

  /**
   * Handle toggle a check box of the Status section
   * @param title
   */
  const handleToggleStatus = (title: string) => {
    setStatusSelections(
      statusSelections.map((selection) => {
        if (selection.title !== title) return selection;
        if (!selection.checked) {
          handleAddChip(title, FilterSections.STATUS);
        } else {
          handleRemoveChip(selection.title);
        }
        return { ...selection, checked: !selection.checked };
      })
    );
  };

  /**
   * Handle toggle selected Loan Officers
   * @param name
   */
  const handleToggleChooseAssigned = (name: string) => {
    setAssignedToSelections(
      assignedToSelections.map((selection) => {
        if (selection.name !== name) return selection;
        if (!selection.selected) {
          handleAddChip(name, FilterSections.ASSIGNED_TO);
        } else {
          handleRemoveChip(selection.name);
        }
        return { ...selection, selected: !selection.selected };
      })
    );
  };

  /**
   * Handle chosing the sort type
   * @param name
   */
  const handleChooseSortType = (index: number) => {
    if (sortBySelections.selected !== -1 && index >= 0) {
      const oldTitle = sortBySelections.selections[sortBySelections.selected];
      const newTitle = sortBySelections.selections[index];
      handleReplaceChipTitle(oldTitle, newTitle);
    } else {
      handleAddChip(sortBySelections.selections[index], FilterSections.SORT_BY);
    }
    setSortBySelections({ ...sortBySelections, selected: index });
  };

  const renderChips = () => {
    return selectedFilters.map((filter, index) => {
      let deleteFunction: (title: string) => void;
      switch (filter.type) {
        case FilterSections.STATUS:
          deleteFunction = handleToggleStatus;
          break;
        case FilterSections.ASSIGNED_TO:
          deleteFunction = handleToggleChooseAssigned;
          break;
        case FilterSections.BEFORE:
          deleteFunction = () => handleEndDate(null);
          break;
        case FilterSections.AFTER:
          deleteFunction = () => handleStartDate(null);
          break;
        case FilterSections.SORT_BY:
          deleteFunction = handleRemoveSortChip;
          break;
        default:
          break;
      }
      return (
        <Grid key={index} item>
          <Chip
            label={`${filter.type}: ${filter.title}`}
            onDelete={() => deleteFunction(filter.title)}
            deleteIcon={<CancelIcon style={{ color: "rgba(0, 0, 0, 0.6)" }} />}
          />
        </Grid>
      );
    });
  };

  const [totalRows, setTotalRows] = React.useState(0);
  const [data, setData] = React.useState<Application[]>([]);
  const [page, setPage] = React.useState(1);
  const perPage = 5;

  const onPaginate = (newPage) => {
    setPage(newPage);
  };

  const [officers, setOfficers] = React.useState([]);

  const fetchOfficers = React.useCallback(() => {
    axios({
      url: `${process.env.REACT_APP_API_ENDPOINT}/api/v1/officers?populate=user`,
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${accessToken}`
      }
    })
      .then((res) => {
        setOfficers(res.data.results);
      })
      .catch((err) => err);
  }, [accessToken]);

  // Load applications from database for page 1
  React.useEffect(() => {
    getApplications(accessToken, page, perPage)
      .then((res) => {
        setData(res.results);
        setTotalRows(res.totalResults);
      })
      .catch((err) => err);
    fetchOfficers();
  }, [page, accessToken, fetchOfficers]);

  const [user, setUser] = React.useState("");

  React.useEffect(() => {
    // TODO: create axios request to set current user
  }, []);

  const columns: ColumnType<Application>[] = [
    {
      title: "Name",
      onSort(newDirection) {
        getApplications(accessToken, page, perPage, `name:${newDirection}`)
          .then((res) => {
            setData(res.results);
            setTotalRows(res.totalResults);
          })
          .catch((err) => err);
      },
      render: (record) => record.firstName + " " + record.lastName
    },
    {
      title: "Date Applied",
      onSort(newDirection) {
        getApplications(accessToken, page, perPage, `createdAt:${newDirection}`)
          .then((res) => {
            setData(res.results);
            setTotalRows(res.totalResults);
          })
          .catch((err) => err);
      },
      render: (record) => moment(record.createdAt).format("MMM D, YYYY")
    },
    {
      title: "Status",
      render: (record) => <StatusDropdown value={record.status} officer={record.officer} />
    },
    {
      title: "Assigned To",
      // TODO: replace currentUser placeholder
      render: (record) => (
        <LoanOfficerDropdown
          officers={officers}
          value={record.officer}
          onChange={(e) => {
            axios({
              url: `${process.env.REACT_APP_API_ENDPOINT}/api/v1/applications/${record.id}`,
              method: "PATCH",
              headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${accessToken}`
              },
              data: {
                officer: e
              }
            })
              .then((res) => {
                if (res.status === 200) {
                  fetchOfficers();
                }
              })
              .catch((err) => {
                console.log(err.message);
              });
          }}
          currentUser="614fc591b172ae06d2ba1e31"
        />
      )
    }
  ];

  return (
    <div className="applications-layout">
      <ThemeProvider theme={theme}>
        <div className="header">
          {pathnames[pathnames.length - 1] !== routes.casesApplications.path && <BackButton pathnames={pathnames} />}
          <Switch>
            <Route exact path={path + "/:id/full"}>
              <TitleTypography key={path} variant="h4" color="textPrimary">
                Client Details
              </TitleTypography>
            </Route>
            <Route exact path={path + routes.createContract.path}>
              <div className="heading">
                <TitleTypography key={path} variant="h4" color="textPrimary">
                  Create Loan Contract
                </TitleTypography>
                <Button
                  color="primary"
                  variant="contained"
                  id="create-btn"
                  onClick={() => {
                    createContractSubmissionRef.current?.handleSubmit();
                  }}
                  // component={RouterLink}
                  // to={`${path}${routes.create.path}`}
                >
                  Save and Create Contract
                </Button>
              </div>
            </Route>
            <Route exact path={path + routes.editContract.path}>
              <div className="heading">
                <TitleTypography key={path} variant="h4" color="textPrimary">
                  Edit and Resend Loan Contract
                </TitleTypography>
                <Button
                  color="primary"
                  variant="contained"
                  id="create-btn"
                  // component={RouterLink}
                  // to={`${path}${routes.create.path}`}
                  onClick={() => {
                    createContractSubmissionRef.current?.handleSubmit();
                  }}
                >
                  Save and Create Contract
                </Button>
              </div>
            </Route>
            <Route>
              <div id="cases-applications-header">
                <TitleTypography variant="h4" color="textPrimary">
                  {breadcrumbMap[pathnames[pathnames.length - 1]]}
                </TitleTypography>
                {location.pathname === routes.casesApplications.path && (
                  <Button
                    color="primary"
                    style={{ color: "white" }}
                    variant="contained"
                    id="add-btn"
                    component={RouterLink}
                    to={`${path}${routes.create.path}`}
                  >
                    <Add style={{ marginRight: "5px" }} />
                    Create application
                  </Button>
                )}
              </div>
            </Route>
          </Switch>
        </div>

        <Switch>
          <Route exact path={path}>
            <Paper className="main" elevation={0} square>
              <ApplicationsFilters
                statusSelections={statusSelections}
                handleToggleStatus={handleToggleStatus}
                sortBySelections={sortBySelections}
                handleChooseSortType={handleChooseSortType}
                assignedToSelections={assignedToSelections}
                handleToggleChooseAssigned={handleToggleChooseAssigned}
                startDate={startDate}
                endDate={endDate}
                setStartDate={setStartDate}
                setEndDate={setEndDate}
                handleApply={handleApplyDate}
              />
              <Grid container spacing={2} className="chips-field">
                {renderChips()}
              </Grid>
              <MyTable
                data={data}
                columns={columns}
                pagination={{
                  perPage,
                  onPaginate,
                  totalRows,
                  page
                }}
                sort={{
                  defaultIndex: 1,
                  defaultDirection: "desc"
                }}
              />
            </Paper>
          </Route>
          <Route exact path={path + "/create"}>
            Create
          </Route>
          <Route path={path + "/:id"}>
            <ApplicationDetails createEditContractSubmissionRef={createContractSubmissionRef} />
          </Route>
        </Switch>
      </ThemeProvider>
    </div>
  );
};

export default withRouter(ApplicationsLayout);
