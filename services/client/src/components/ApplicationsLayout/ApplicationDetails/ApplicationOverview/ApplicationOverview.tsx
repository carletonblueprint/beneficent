import * as React from "react";
import { Button, Grid, Card, CardHeader, CardContent, Typography } from "@material-ui/core";

import "./ApplicationOverview.scss";

const ApplicationOverview = (props) => {
  const { values } = props;
  return (
    <Card className="application-overview" style={{ width: "1065px" }}>
      <div id="application-overview-header">
        <CardHeader title="Application Overview">Application Overview</CardHeader>
        <Button id="app-ovr-view" color="primary" style={{ color: "white" }} variant="contained">
          View
        </Button>
      </div>

      <hr id="ao-line" />
      <CardContent>
        <Grid container direction="row" spacing={1} style={{ position: "relative" }}>
          <Grid item container direction="column" spacing={5} xs={6} style={{ width: "303px", flexBasis: "auto" }}>
            <Grid item>
              <Typography variant="body1" className="application-overview-label">
                <b>Citizenship</b>
              </Typography>
              <Typography variant="body1" className="application-overview-value">
                Canadian
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant="body1" className="application-overview-label">
                <b>Employement Status</b>
              </Typography>
              <Typography variant="body1" className="application-overview-value">
                Unemployed
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant="body1" className="application-overview-label">
                <b>Amount Requested</b>
              </Typography>
              <Typography variant="body1" className="application-overview-value">
                $0000.00
              </Typography>
            </Grid>
          </Grid>
          <Grid item container direction="column" spacing={5} xs={6} style={{ width: "225px", flexBasis: "auto" }}>
            <Grid item>
              <Typography variant="body1" className="application-overview-label">
                <b>Loan Type</b>
              </Typography>
              <Typography variant="body1" className="application-overview-value">
                xyx loan
              </Typography>
            </Grid>
            <Grid item>
              <Typography variant="body1" className="application-overview-label">
                <b>Guarantor</b>
              </Typography>
              <Typography variant="body1" className="application-overview-value">
                Yes
              </Typography>
            </Grid>
          </Grid>
          <Grid
            item
            container
            direction="column"
            spacing={5}
            xs={6}
            id="application-overview-acknowledgements"
            style={{ width: "650px", flexBasis: "auto" }}
          >
            <Grid item style={{ width: "650px" }}>
              <Typography variant="body1" className="citizenship-label">
                <b>Acknowledgments</b>
              </Typography>
              <div className="acknowledgement-check">
                <input checked type="checkbox" />
                <span style={{ paddingTop: "5px" }}>
                  Loan purpose is for credit card, emergency, or similar high interest bearing debt
                </span>
              </div>
              <div className="acknowledgement-check">
                <input checked type="checkbox" />
                <span style={{ paddingTop: "5px" }}>Maximum loan value of $4,000</span>
              </div>
              <div className="acknowledgement-check">
                <input checked type="checkbox" />
                <span style={{ paddingTop: "5px" }}>Repayment within 12 months</span>
              </div>
              <div className="acknowledgement-check">
                <input checked type="checkbox" />
                <span style={{ paddingTop: "5px" }}>Residence in Canada</span>
              </div>
              <div className="acknowledgement-check">
                <input checked type="checkbox" />
                <span style={{ paddingTop: "5px" }}>Positive net income</span>
              </div>
              <div className="acknowledgement-check">
                <input checked type="checkbox" />
                <span style={{ paddingTop: "5px" }}>Guarantor consent</span>
              </div>
            </Grid>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

export default ApplicationOverview;
