const path = require("path");

const objectId = (value, helpers) => {
  if (!value.match(/^[0-9a-fA-F]{24}$/)) {
    return helpers.message('"{{#label}}" must be a valid mongo id');
  }
  return value;
};

const password = (value, helpers) => {
  if (value.length < 8) {
    return helpers.message("password must be at least 8 characters");
  }
  if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
    return helpers.message("password must contain at least 1 letter and 1 number");
  }
  return value;
};

const fileExtension = (value, helpers) => {
  const validFileExts = [".pdf", ".jpeg", ".jpg", ".png"];
  const ext = path.extname(value);
  if (!validFileExts.includes(ext)) {
    return helpers.message("only .pdf, .jpeg, .jpg, and .png files are allowed");
  }
  return value;
};

const contractFileExtension = (value, helpers) => {
  const validFileExt = ".pdf";
  const ext = path.extname(value);
  if (ext !== validFileExt) {
    return helpers.message("only .pdf files are allowed");
  }
  return value;
};

module.exports = {
  objectId,
  password,
  fileExtension,
  contractFileExtension
};
