import { Grid, Paper } from "@material-ui/core";
import * as React from "react";
import { RefObject, useEffect, useState } from "react";
import "./ApplicationDetails.scss";
import { Route, Switch, useParams } from "react-router-dom";
import routes from "../../../routes";
import CreateEditContract from "./CreateEditContract/CreateEditContract";
import ApplicationOverview from "./ApplicationOverview/ApplicationOverview";
import { FormikSubmitObject } from "../ApplicationsLayout";
import axios, { AxiosRequestConfig } from "axios";

interface ApplicationDetailsProps {
  createEditContractSubmissionRef: RefObject<FormikSubmitObject>;
}

interface ApplicationInfo {
  id: string;
  name: string;
  email: string;
  phoneNumber: string;
  address: string;
  city: string;
  province: string;
  sex: string;
  dateOfBirth: string;
  maritalStatus: string;
  citizenship: string;
  employmentStatus: string;
  loanAmountRequested: number;
  loanType: string;
  debtCircumstances: string;
  guarantor: string;
  recommendationInfo: string;
  status: string;
  officer: string;
  user: string;
}

/**
 * Detail page of a client
 */
const ApplicationDetails = (props: ApplicationDetailsProps) => {
  const { createEditContractSubmissionRef } = props;
  const { id } = useParams<{ id: string }>();
  const token = window.localStorage.getItem("accessToken");
  const [applicationInfo, setApplicationInfo] = useState<ApplicationInfo>();

  // TODO: Add redirection if the user-id is not available
  useEffect(() => {
    const options: AxiosRequestConfig = {
      url: `${process.env.REACT_APP_API_ENDPOINT}/api/v1/applications/${id}`,
      method: "get",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
      }
    };

    axios(options)
      .then((res) => {
        setApplicationInfo(res.data);
      })
      .catch((err) => {
        // console.log(err.message);
      });
  }, [id, token]);

  return (
    <Grid className="application-details-container" spacing={3} container data-testid="skeleton-container">
      <Switch>
        <Route exact path={routes.casesApplications.path + "/:id"}>
          <Grid spacing={2} item container direction="row">
            <Grid item xs={12}>
              {/* TODO Replace this paper by <ApplicationInformation /> component */}
              <Paper className="application-details-box" elevation={1}>
                Application Information
              </Paper>
            </Grid>
            <Grid item xs={12}>
              <ApplicationOverview></ApplicationOverview>
            </Grid>
            <Grid item xs={12}>
              {/* TODO Replace this paper by <ApplicationDocuments /> component */}
              <Paper className="application-details-box" elevation={1}>
                Applicaiton Documents
              </Paper>
            </Grid>
            <Grid item xs={12}>
              {/* TODO Replace this paper by <ContractsBox /> component */}
              <Paper className="application-details-box" elevation={1}>
                Application Contracts
              </Paper>
            </Grid>
          </Grid>
        </Route>
        <Route exact path={routes.casesApplications.path + "/:id/full"}>
          <Grid item container>
            {/* TODO Replace the line below by <ApplicationFullDetails /> component*/}
            Application Full Details
          </Grid>
        </Route>
        <Route exact path={routes.casesApplications.path + routes.createContract.path}>
          {/* TODO Replace this paper by <EditContract /> component */}
          <Grid item container>
            <CreateEditContract
              ref={createEditContractSubmissionRef}
              guarantor={applicationInfo?.guarantor}
              officerId={applicationInfo?.officer}
              userId={applicationInfo?.user}
            />
          </Grid>
        </Route>
        <Route exact path={routes.casesApplications.path + routes.editContract.path}>
          {/* TODO Replace this paper by <EditContract /> component */}
          <Grid item container>
            {/* TODO Replace the line below by <EditContract /> component*/}
            Edit Contract
          </Grid>
        </Route>
      </Switch>
    </Grid>
  );
};

export default ApplicationDetails;
