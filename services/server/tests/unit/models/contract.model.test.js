const faker = require("faker");
const mongoose = require("mongoose");
const { Contract } = require("../../../src/models");
const { dateOne, dateTwo, dateThree } = require("../../fixtures/contract.fixture");

describe("Contract model", () => {
  describe("Contract validation", () => {
    let newContract;

    beforeEach(() => {
      newContract = {
        guarantorName: faker.name.findName(),
        approvedLoanAmount: 1500,
        firstPaymentDue: dateOne,
        finalPaymentDue: dateTwo,
        status: "Draft",
        contractStartDate: dateThree,
        client: mongoose.Types.ObjectId()
      };
    });

    test("should correctly validate a valid contract", async () => {
      await expect(new Contract(newContract).validate()).resolves.toBeUndefined();
    });

    test("should throw a validation error if firstPaymentDue is invalid", async () => {
      newContract.firstPaymentDue = "invalidDate";
      await expect(new Contract(newContract).validate()).rejects.toThrow();
    });

    test("should throw a validation error if finalPaymentDue is invalid", async () => {
      newContract.finalPaymentDue = "invalidDate";
      await expect(new Contract(newContract).validate()).rejects.toThrow();
    });

    test("should throw a validation error if client is invalid", async () => {
      newContract.client = "invalidClient";
      await expect(new Contract(newContract).validate()).rejects.toThrow();
    });
  });
});
