const faker = require("faker");
const { User } = require("../../../src/models");
const getRandomPhoneNumber = require("../../fixtures/phoneNumber.fixture");

describe("User model", () => {
  describe("User validation", () => {
    let newUser;
    beforeEach(() => {
      newUser = {
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        phoneNumber: getRandomPhoneNumber(),
        address: faker.address.streetAddress(),
        country: "Canada",
        email: faker.internet.email().toLowerCase(),
        password: "password1",
        role: "user"
      };
    });

    test("should correctly validate a valid user", async () => {
      await expect(new User(newUser).validate()).resolves.toBeUndefined();
    });

    test("should throw a validation error if email is invalid", async () => {
      newUser.email = "invalidEmail";
      await expect(new User(newUser).validate()).rejects.toThrow();
    });

    test("should throw a validation error if password length is less than 8 characters", async () => {
      newUser.password = "passwo1";
      await expect(new User(newUser).validate()).rejects.toThrow();
    });

    test("should throw a validation error if password does not contain numbers", async () => {
      newUser.password = "password";
      await expect(new User(newUser).validate()).rejects.toThrow();
    });

    test("should throw a validation error if password does not contain letters", async () => {
      newUser.password = "11111111";
      await expect(new User(newUser).validate()).rejects.toThrow();
    });

    test("should throw a validation error if role is unknown", async () => {
      newUser.role = "invalid";
      await expect(new User(newUser).validate()).rejects.toThrow();
    });
    //------------
    test("should throw validation error if phone number in too long", async () => {
      newUser.phoneNumber = "(123232))34765738463-387634691";
      await expect(new User(newUser).validate()).rejects.toThrow();
    });

    test("should throw validation error if phone number in wrong format", async () => {
      newUser.phoneNumber = "(123)123 -1231";
      await expect(new User(newUser).validate()).rejects.toThrow();
    });

    test("should throw validation error if phone number has invalid characters", async () => {
      newUser.phoneNumber = "(abc) DeF-GHij";
      await expect(new User(newUser).validate()).rejects.toThrow();
    });

    test("should correctly validate a user", async () => {
      newUser.phoneNumber = "(613) 979-8818";
      await expect(new User(newUser).validate()).resolves.toBeUndefined();
    });
  });

  describe("User toJSON()", () => {
    test("should not return user password when toJSON is called", () => {
      const newUser = {
        name: faker.name.findName(),
        phoneNumber: getRandomPhoneNumber(),
        address: faker.address.streetAddress(),
        country: "Canada",
        email: faker.internet.email().toLowerCase(),
        password: "password1",
        role: "user"
      };
      expect(new User(newUser).toJSON()).not.toHaveProperty("password");
    });
  });
});
