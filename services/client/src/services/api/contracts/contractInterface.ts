export interface createContract {
  guarantorName: string;
  approvedLoanAmount: number;
  firstPaymentDue: string;
  finalPaymentDue: string;
  monthlyPayment: number;
  finalPayment: number;
  status: string;
  client: string;
}

export interface getContracts {
  application?: string;
  client?: string;
  officer?: string;
  populate?: string;
  sortBy?: string;
  limit?: number;
  page?: number;
}

export interface updateContract {
  guarantorName?: string;
  approvedLoanAmount?: string;
  firstPaymentDue?: string;
  finalPaymentDue?: string;
  monthlyPayment?: number;
  finalPayment?: number;
  status?: string;
  client?: string;
}
