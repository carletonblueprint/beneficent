import React from "react";
import { Route, Switch, useHistory } from "react-router-dom";

import SignUp from "./components/SignUp/SignUp";
import Login from "./components/Login/Login";
import PasswordReset from "./components/PasswordReset/PasswordReset";
import PasswordResetRequest from "./components/PasswordResetRequest/PasswordResetRequest";
import AdminHomePage from "./components/HomePage/AdminHomePage/AdminHomePage";

// TODO Change this to specific Account settings page for according user types
import ROUTES from "./routes";
import axios from "axios";

type Account = {
  email: string;
  password: string;
};

/**
 * The main component of the application
 * ** It contains information about user's authentication
 */
const App = () => {
  /**
   * The history controlling the page routes
   */
  const history = useHistory();

  /**
   * Check authentication of every refresh of the page and reset the token
   */
  const validRefresh = async () => {
    try {
      const token = window.localStorage.getItem("refreshToken");
      const url = `${process.env.REACT_APP_API_ENDPOINT}/api/v1/auth/refresh-tokens`;
      const res = await axios.post(url, {
        refreshToken: token
      });
      window.localStorage.setItem("refreshToken", res.data.refresh.token);
      window.localStorage.setItem("accessToken", res.data.access.token);
      return true;
    } catch (err) {
      console.log(err);
      return false;
    }
  };

  /**
   * Validate the account to server to get login token
   * @param account an Object contains 'email' and 'password' properties
   */
  const handleSubmitLoginForm = async (account: Account) => {
    try {
      const url = `${process.env.REACT_APP_API_ENDPOINT}/api/v1/auth/login`;
      const response = await axios.post(url, account);
      window.localStorage.setItem("refreshToken", response.data.tokens.refresh.token);
      window.localStorage.setItem("accessToken", response.data.tokens.access.token);
      history.push(ROUTES.home.path);
    } catch (err) {
      throw new Error("Incorrect email or password. Please try again.");
    }
  };

  /**
   * Logout the current user by remove access and
   * refresh token then redirect to login page
   */
  const handleLogout = () => {
    window.localStorage.removeItem("refreshToken");
    window.localStorage.removeItem("accessToken");
    history.push(ROUTES.login.path);
  };

  const handleSubmitSignUpForm = async (values: Object) => {};

  return (
    <Switch>
      <Route
        exact={ROUTES.login.exact}
        path={ROUTES.login.path}
        render={() => <Login handleSubmitLoginForm={handleSubmitLoginForm} validRefresh={validRefresh} />}
      />

      <Route exact={ROUTES.resetPassword.exact} path={ROUTES.resetPassword.path} render={() => <PasswordReset />} />

      <Route
        exact={ROUTES.requestResetPassword.exact}
        path={ROUTES.requestResetPassword.path}
        render={() => <PasswordResetRequest />}
      />

      <Route
        exact={ROUTES.signup.exact}
        path={ROUTES.signup.path}
        render={() => <SignUp handleSubmitSignUpForm={handleSubmitSignUpForm} />}
      />

      <Route exact={ROUTES.contact.exact} path={ROUTES.contact.path} render={() => <div>Contact us</div>} />

      <Route
        exact={ROUTES.home.exact}
        path={ROUTES.home.path}
        render={() => <AdminHomePage handleLogout={handleLogout} validRefresh={validRefresh} />}
      />
    </Switch>
  );
};

export default App;
