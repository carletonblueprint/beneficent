const Joi = require("joi");

const commandSchema = Joi.object({
  service: Joi.string().valid("server", "client", "util", "git").required(),
  command: Joi.string()
    .valid(
      "start",
      "seed",
      "test",
      "update-snapshots",
      "coverage",
      "style:check",
      "style:fix",
      "commit",
      "clean",
      "setup",
      "migrate",
      "debug"
    )
    .required()
});

module.exports = commandSchema;
