const httpStatus = require("http-status");
const { Officer, User } = require("../models");
const ApiError = require("../utils/ApiError");
const userService = require("./user.service");

/**
 * Create a officer
 * @param {Object} officerBody
 * @returns {Promise<Officer>}
 */
const createOfficer = async (officerBody) => {
  const user = await User.findById(officerBody.user);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, "User with this user id does not exist");
  }
  if (await Officer.isDuplicate(officerBody.user)) {
    throw new ApiError(httpStatus.BAD_REQUEST, "Officer with this user ID already exists");
  }
  return Officer.create(officerBody);
};

/**
 * Query for officers
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.populate] - Populate data fields. Hierarchy of fields should be separated by (.). Multiple populating criteria should be separated by commas (,)
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryOfficers = async (filter, options) => {
  let userFilter = {};
  let officerFilter = {};
  if (filter.firstName) userFilter.firstName = { $regex: `${filter.firstName}`, $options: "i" };
  if (filter.lastName) userFilter.lastName = { $regex: `${filter.lastName}`, $options: "i" };
  if (filter.email) userFilter.email = filter.email;
  if (filter.user) userFilter._id = filter.user;
  const users = await User.find(userFilter);

  let ids = [];
  users.forEach((user) => {
    ids.push(user.id);
  });
  officerFilter = {
    $and: [{ user: { $in: ids } }]
  };

  const officers = await Officer.paginate(officerFilter, options);
  if (officers.results.length == 0) {
    throw new ApiError(httpStatus.NOT_FOUND, "No officers found");
  }
  return officers;
};

/**
 * Get officer by id
 * @param {ObjectId} officerId
 * @returns {Promise<Officer>}
 */
const getOfficerById = async (officerId) => {
  return Officer.findById(officerId);
};

/**
 * Update officer by id
 * @param {ObjectId} officerId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const updateOfficerById = async (officerId, updateBody) => {
  const officer = await getOfficerById(officerId);
  if (!officer) {
    throw new ApiError(httpStatus.NOT_FOUND, "Officer not found");
  }

  const user = await userService.getUserById(updateBody.user);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, "User not found");
  }

  Object.assign(officer, updateBody);
  await officer.save();
  return officer;
};

/**
 * Delete officer by id
 * @param {ObjectId} officerId
 * @returns {Promise<User>}
 */
const deleteOfficerById = async (officerId) => {
  const officer = await getOfficerById(officerId);
  if (!officer) {
    throw new ApiError(httpStatus.NOT_FOUND, "Officer not found");
  }
  await officer.remove();
  return officer;
};

module.exports = {
  createOfficer,
  queryOfficers,
  getOfficerById,
  updateOfficerById,
  deleteOfficerById
};
