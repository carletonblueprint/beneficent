import axios, { AxiosError, AxiosResponse } from "axios";
import { GetModel, UpdateModel, CreateModel, UploadModel } from "./types";

export const BASE_URL = `${process.env.REACT_APP_API_ENDPOINT}/api/v1`;

/** @param {string} resource*/
/** @param {string} token */
/** @param {GetModel} model */
const getAll = (
  resource: string,
  token: string,
  model?: GetModel
): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
  let urlPath = resource;
  if (model !== undefined) {
    urlPath += "?" + new URLSearchParams(model[resource]);
  }
  return axios
    .get(`${BASE_URL}/${urlPath}`, generateHeaders(token))
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return err.response.data;
    });
};

/** @param {string} resource */
/** @param {string} id */
const getSingle = (resource: string, token: string, id: string): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
  return axios
    .get(`${BASE_URL}/${resource}/${id}`, generateHeaders(token))
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return err.response.data;
    });
};

/** @param {string} resource */
/** @param {string} token */
/** @param {CreateModel} model */
const post = (
  resource: string,
  token: string,
  model: CreateModel
): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
  return axios
    .post(`${BASE_URL}/${resource}`, model[resource], generateHeaders(token))
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return err.response.data;
    });
};

/** @param {string} resource */
/** @param {string} token */
/** @param {string} id */
/** @param {UpdateModel} model */
const patch = (
  resource: string,
  token: string,
  id: string,
  model: UpdateModel
): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
  return axios
    .patch(`${BASE_URL}/${resource}/${id}`, model[resource], generateHeaders(token))
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return err.response.data;
    });
};

/** @param {string} resource */
/** @param {string} token */
/** @param {string} id */
const remove = (resource: string, token: string, id: string): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
  return axios
    .delete(`${BASE_URL}/${resource}/${id}`, generateHeaders(token))
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return err.response.data;
    });
};

/** @param {string} resource */
/** @param {string} token */
/** @param {UploadModel} model */
const upload = (
  resource: string,
  token: string,
  model: UploadModel
): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
  const formData = new FormData();
  for (const key in model[resource]) {
    formData.append(key, model[resource][key]);
  }
  return axios
    .post(`${BASE_URL}/${resource}/upload`, formData, {
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${token}`
      }
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response.data;
    });
};

/** @param {string} resource */
/** @param {string} token */
/** @param {string} id */
const download = (resource: string, token: string, id: string): Promise<AxiosResponse<any>> | Promise<AxiosError<any>> => {
  return axios
    .get(`${BASE_URL}/${resource}/${id}/download`, generateHeaders(token))
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err.response.data;
    });
};

/** @param {string} token */
/** @param {ApiModel} model */
const generateHeaders = (token: string): any => {
  return {
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`
    }
  };
};

export const apiProvider = {
  getAll,
  getSingle,
  post,
  patch,
  remove,
  upload,
  download,
  generateHeaders
};
