import { createApplication, updateApplication, getApplications } from "../applications/applicationInterface";
import { createClient, getClients, updateClient } from "../clients/clientInterface";
import { createContract, updateContract, getContracts } from "../contracts/contractInterface";

import { getDocuments, uploadDocument } from "../documents/documentInterface";

import { createOfficer, updateOfficer, getOfficers } from "../officers/officerInterface";
import { createUser, updateUser, getUsers } from "../users/userInterface";

export interface CreateModel {
  contracts?: createContract;
  users?: createUser;
  clients?: createClient;
  applications?: createApplication;
  officers?: createOfficer;
}

export interface GetModel {
  contracts?: getContracts;
  users?: getUsers;
  clients?: getClients;
  applications?: getApplications;
  officers?: getOfficers;
  documents?: getDocuments;
}

export interface UpdateModel {
  users?: updateUser;
  clients?: updateClient;
  contracts?: updateContract;
  applications?: updateApplication;
  officers?: updateOfficer;
}

export interface ApiModel {
  createModel?: CreateModel;
  getModel?: GetModel;
  updateModel?: UpdateModel;
}

export interface UploadModel {
  documents?: uploadDocument;
}
