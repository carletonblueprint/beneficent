const request = require("supertest");
const httpStatus = require("http-status");
const app = require("../../src/app");
const path = require("path");
const mongoose = require("mongoose");
const setupTestDB = require("../utils/setupTestDB");
const { Document } = require("../../src/models");
const { applicationOne, insertApplications, applicationTwo } = require("../fixtures/application.fixture");
const { userOne, officerUserOne, insertUsers } = require("../fixtures/user.fixture");
const { userOneAccessToken, officerOneAccessToken } = require("../fixtures/token.fixture");
const { documentOne, documentTwo, insertDocuments } = require("../fixtures/document.fixture");

setupTestDB();

describe("Document routes", () => {
  describe("POST /api/v1/documents", () => {
    let body;
    beforeEach(async () => {
      body = {
        docType: "Other",
        application: applicationOne._id
      };
    });

    test("should return 201 and the document", async () => {
      insertUsers([officerUserOne]);
      insertApplications([applicationOne]);

      const res = await request(app)
        .post("/api/v1/documents")
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .set("Content-type", "multipart/form-data")
        .field("docType", body.docType)
        .field("application", `${body.application}`)
        .attach("file", path.join(__dirname, "../fixtures/files/validFile.pdf"))
        .expect(httpStatus.CREATED);

      expect(res.body).toEqual({
        id: expect.anything(),
        docType: body.docType,
        originalName: "validFile.pdf",
        mimeType: "application/pdf",
        size: expect.anything(),
        application: applicationOne._id.toHexString(),
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });

      const dbDocument = await Document.findById(res.body.id);
      expect(dbDocument).toBeDefined();
      expect(dbDocument).toMatchObject({
        ...body,
        id: expect.anything(),
        originalName: "validFile.pdf",
        mimeType: "application/pdf",
        size: "5000",
        buffer: expect.anything(),
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
    });

    test("should return 401 if access token is missing", async () => {
      const res = await request(app)
        .post("/api/v1/documents")
        .set("Content-type", "multipart/form-data")
        .field("docType", body.docType)
        .field("application", `${body.application}`)
        .expect(httpStatus.UNAUTHORIZED);

      expect(res.body).toMatchObject({
        message: "Please authenticate"
      });
    });

    test("should return 403 if a non-admin is trying to upload a document", async () => {
      insertUsers([userOne]);
      insertApplications([applicationOne]);

      const res = await request(app)
        .post("/api/v1/documents")
        .set("Content-type", "multipart/form-data")
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .field("docType", body.docType)
        .field("application", `${body.application}`)
        .attach("file", path.join(__dirname, "../fixtures/files/validFile.pdf"))
        .expect(httpStatus.FORBIDDEN);

      expect(res.body).toMatchObject({
        message: "Forbidden"
      });
    });

    test("should return 400 if document extension is invalid", async () => {
      insertUsers([officerUserOne]);
      insertApplications([applicationOne]);

      const res = await request(app)
        .post("/api/v1/documents")
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .set("Content-type", "multipart/form-data")
        .field("docType", body.docType)
        .field("application", `${body.application}`)
        .attach("file", path.join(__dirname, "../fixtures/files/invalidExt.doc"))
        .expect(httpStatus.BAD_REQUEST);

      expect(res.body).toMatchObject({
        message: "only .pdf, .jpeg, .jpg, and .png files are allowed"
      });
    });

    test("should return 500 if document larger than 10MB", async () => {
      jest.setTimeout(30000); // Set longer timeout for large file upload
      await insertUsers([officerUserOne]);
      await insertApplications([applicationOne]);

      const res = await request(app)
        .post("/api/v1/documents")
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .set("Content-type", "multipart/form-data")
        .field("docType", body.docType)
        .field("application", `${body.application}`)
        .attach("file", path.join(__dirname, "../fixtures/files/fileTooLarge.pdf"))
        .expect(httpStatus.INTERNAL_SERVER_ERROR);

      expect(res.body).toMatchObject({
        message: "File too large"
      });
    });

    test("should return 404 if the application ID does not exist", async () => {
      insertUsers([officerUserOne]);
      insertApplications([applicationOne]);

      const res = await request(app)
        .post("/api/v1/documents")
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .set("Content-type", "multipart/form-data")
        .field("docType", body.docType)
        .field("application", `${mongoose.Types.ObjectId()}`)
        .attach("file", path.join(__dirname, "../fixtures/files/validFile.pdf"))
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({
        message: "Application with this ID does not exist"
      });
    });

    test("should return 400 if docType is invalid", async () => {
      insertUsers([officerUserOne]);
      insertApplications([applicationOne]);

      const res = await request(app)
        .post("/api/v1/documents")
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .set("Content-type", "multipart/form-data")
        .field("docType", "invalidDocType")
        .field("application", `${body.application}`)
        .attach("file", path.join(__dirname, "../fixtures/files/validFile.pdf"))
        .expect(httpStatus.BAD_REQUEST);

      expect(res.body).toMatchObject({
        message: '"docType" must be one of [Identification, Proof of Debt, Proof of Income, Other]'
      });
    });
  });

  describe("GET /api/v1/documents", () => {
    test("should return 200 and apply the default query options", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertDocuments([documentOne, documentTwo]);
      await insertUsers([officerUserOne]);

      const res = await request(app)
        .get("/api/v1/documents")
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 2
      });

      expect(res.body.results).toHaveLength(2);
      expect(res.body.results[0]).toEqual({
        id: documentOne._id.toHexString(),
        docType: documentOne.docType,
        originalName: documentOne.originalName,
        application: documentOne.application.toHexString(),
        size: documentOne.size.toString(),
        mimeType: documentOne.mimeType,
        createdAt: expect.anything(),
        updatedAt: expect.anything()
      });
    });

    test("should return 401 if access token is missing", async () => {
      await insertDocuments([documentOne, documentTwo]);
      await request(app).get("/api/v1/documents").send().expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if a non-admin is trying to access all documents", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertDocuments([documentOne, documentTwo]);
      await insertUsers([userOne]);

      await request(app)
        .get("/api/v1/documents")
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test("should correctly apply filter on docType field", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertDocuments([documentOne, documentTwo]);
      await insertUsers([officerUserOne]);

      const res = await request(app)
        .get("/api/v1/documents")
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .query({ docType: documentOne.docType })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(documentOne._id.toHexString());
    });

    test("should correctly apply filter on originalName field", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertDocuments([documentOne, documentTwo]);
      await insertUsers([officerUserOne]);

      const res = await request(app)
        .get("/api/v1/documents")
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .query({ originalName: documentOne.originalName })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 2
      });
      expect(res.body.results).toHaveLength(2);
      expect(res.body.results[0].id).toBe(documentOne._id.toHexString());
    });

    test("should correctly apply filter on application field", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertDocuments([documentOne, documentTwo]);
      await insertUsers([officerUserOne]);

      const res = await request(app)
        .get("/api/v1/documents")
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .query({ application: documentOne.application.toHexString() })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 10,
        totalPages: 1,
        totalResults: 1
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(documentOne._id.toHexString());
    });

    test("should return 404 if no documents are found", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertDocuments([documentOne, documentTwo]);
      await insertUsers([officerUserOne]);

      const res = await request(app)
        .get("/api/v1/documents")
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .query({ docType: "doesNotExist" })
        .send()
        .expect(httpStatus.NOT_FOUND);

      expect(res.body).toMatchObject({ message: "No documents found" });
    });

    test("should limit returned array if limit param is specified", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertDocuments([documentOne, documentTwo]);
      await insertUsers([officerUserOne]);

      const res = await request(app)
        .get("/api/v1/documents")
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .query({ limit: 1 })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 1,
        limit: 1,
        totalPages: 2,
        totalResults: 2
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(documentOne._id.toHexString());
    });

    test("should return the correct page if page and limit params are specified", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertDocuments([documentOne, documentTwo]);
      await insertUsers([officerUserOne]);

      const res = await request(app)
        .get("/api/v1/documents")
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .query({ page: 2, limit: 1 })
        .send()
        .expect(httpStatus.OK);

      expect(res.body).toEqual({
        results: expect.any(Array),
        page: 2,
        limit: 1,
        totalPages: 2,
        totalResults: 2
      });
      expect(res.body.results).toHaveLength(1);
      expect(res.body.results[0].id).toBe(documentTwo._id.toHexString());
    });
  });

  describe("GET /api/v1/documents/:documentId", () => {
    test("should return 200 and the document file  if data is ok", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertDocuments([documentOne, documentTwo]);
      await insertUsers([officerUserOne]);

      const res = await request(app)
        .get(`/api/v1/documents/${documentOne._id}`)
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .send()
        .expect(httpStatus.OK);

      const expectedFileName = documentOne.originalName;
      expect(res.headers["content-disposition"]).toBe(`attachment; filename=${expectedFileName}`);
      expect(res.headers["content-type"]).toBe("application/octet-stream");
    });

    test("should return 400 if documentId is not a valid mongo id", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertDocuments([documentOne, documentTwo]);
      await insertUsers([officerUserOne]);

      await request(app)
        .get("/api/v1/contracts/invalidId")
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .send()
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 401 if access token is missing", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertDocuments([documentOne, documentTwo]);
      await request(app).get(`/api/v1/documents/${documentOne._id}`).send().expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if a non-admin is trying to download a document", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertDocuments([documentOne, documentTwo]);
      await insertUsers([userOne]);

      await request(app)
        .get(`/api/v1/documents/${documentOne._id}`)
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 404 if document is not found", async () => {
      await insertUsers([officerUserOne]);
      await request(app)
        .get(`/api/v1/documents/${documentOne._id}`)
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);
    });
  });

  describe("DELETE /api/v1/documents/:documentId", () => {
    test("should return 204 if data is ok", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertDocuments([documentOne, documentTwo]);
      await insertUsers([officerUserOne]);

      await request(app)
        .delete(`/api/v1/documents/${documentOne._id}`)
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .send()
        .expect(httpStatus.NO_CONTENT);

      const dbDocument = await Document.findById(documentOne._id);
      expect(dbDocument).toBeNull();
    });

    test("should return 401 if access token is missing", async () => {
      await insertApplications([applicationOne, applicationTwo]);
      await insertDocuments([documentOne, documentTwo]);

      await request(app).delete(`/api/v1/documents/${documentOne._id}`).send().expect(httpStatus.UNAUTHORIZED);
    });

    test("should return 403 if a non-admin is trying to delete a document", async () => {
      await insertUsers([userOne]);
      await insertApplications([applicationOne, applicationTwo]);
      await insertDocuments([documentOne, documentTwo]);

      await request(app)
        .delete(`/api/v1/documents/${documentOne._id}`)
        .set("Authorization", `Bearer ${userOneAccessToken}`)
        .send()
        .expect(httpStatus.FORBIDDEN);
    });

    test("should return 400 if documentId is not a valid mongo id", async () => {
      await insertUsers([officerUserOne]);

      await request(app)
        .delete("/api/v1/documents/invalidId")
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .send()
        .expect(httpStatus.BAD_REQUEST);
    });

    test("should return 404 if document is not found", async () => {
      await insertUsers([officerUserOne]);

      const res = await request(app)
        .delete(`/api/v1/documents/${documentOne._id}`)
        .set("Authorization", `Bearer ${officerOneAccessToken}`)
        .send()
        .expect(httpStatus.NOT_FOUND);

      expect(res.body.message).toEqual("Document not found");
    });
  });
});
