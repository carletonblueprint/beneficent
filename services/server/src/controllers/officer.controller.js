const httpStatus = require("http-status");
const pick = require("../utils/pick");
const ApiError = require("../utils/ApiError");
const catchAsync = require("../utils/catchAsync");
const { officerService } = require("../services");

const createOfficer = catchAsync(async (req, res) => {
  const officer = await officerService.createOfficer(req.body);
  res.status(httpStatus.CREATED).send(officer);
});

const getOfficers = catchAsync(async (req, res) => {
  const filter = pick(req.query, ["firstName", "lastName", "email", "user"]);
  const options = pick(req.query, ["populate", "sortBy", "limit", "page"]);
  const result = await officerService.queryOfficers(filter, options);
  res.send(result);
});

const getOfficer = catchAsync(async (req, res) => {
  const officer = await officerService.getOfficerById(req.params.officerId);
  if (!officer) {
    throw new ApiError(httpStatus.NOT_FOUND, "Officer not found");
  }
  res.send(officer);
});

const updateOfficer = catchAsync(async (req, res) => {
  const officer = await officerService.updateOfficerById(req.params.officerId, req.body);
  res.send(officer);
});

const deleteOfficer = catchAsync(async (req, res) => {
  await officerService.deleteOfficerById(req.params.officerId);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createOfficer,
  getOfficers,
  getOfficer,
  updateOfficer,
  deleteOfficer
};
