# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [7.0.0](https://gitlab.com/carletonblueprint/beneficent/compare/v6.1.1...v7.0.0) (2021-09-08)


### ⚠ BREAKING CHANGES

* Add has gurantor field (EN-372)

### Features

* Add has gurantor field (EN-372) ([2cff768](https://gitlab.com/carletonblueprint/beneficent/commit/2cff7689d37a8aab54d63db0e9bfc8894c12621d))

### [6.1.1](https://gitlab.com/carletonblueprint/beneficent/compare/v6.1.0...v6.1.1) (2021-09-01)


### Bug Fixes

* update fixture workflows (EN-353) ([ec6fc31](https://gitlab.com/carletonblueprint/beneficent/commit/ec6fc3120513a70b65d342558ee34c377e77ce6f))

## [6.1.0](https://gitlab.com/carletonblueprint/beneficent/compare/v6.0.0...v6.1.0) (2021-08-25)


### Features

* implement contract downloads and update contract start date (EN-351, EN-352) ([83cb065](https://gitlab.com/carletonblueprint/beneficent/commit/83cb065844652a017505cac44068dfdc6a5ca500))


### Bug Fixes

* officer workload not being calculated properly (EN-357) ([7a760d9](https://gitlab.com/carletonblueprint/beneficent/commit/7a760d91080d21c43b8bf6e48466697eab46ad1f))

## [6.0.0](https://gitlab.com/carletonblueprint/beneficent/compare/v5.0.0...v6.0.0) (2021-08-18)


### ⚠ BREAKING CHANGES

* fix workflow (EN-368)

### Bug Fixes

* fix workflow (EN-368) ([3d2d652](https://gitlab.com/carletonblueprint/beneficent/commit/3d2d65249dfa27f95e5854b2914d199225e7cada))
* update paths for lint-staged (EN-361) ([5cec17b](https://gitlab.com/carletonblueprint/beneficent/commit/5cec17bdfc67be5cb7a103fb698b38d98edbd1e1))

## [5.0.0](https://gitlab.com/carletonblueprint/beneficent/compare/v4.1.0...v5.0.0) (2021-08-11)


### ⚠ BREAKING CHANGES

* hide active clients (EN-340)
* refactor model references (EN-349)

### Features

* add GET and DELETE to documents route (EN-332) ([e3cc1ac](https://gitlab.com/carletonblueprint/beneficent/commit/e3cc1ac4ca6d57d06fd0a8dead1277abeb1ac2d1))
* refactor model references (EN-349) ([7572526](https://gitlab.com/carletonblueprint/beneficent/commit/757252666db0076faf7dc72e52dbcb14cef3fc10))


### Bug Fixes

* hide active clients (EN-340) ([a3cc837](https://gitlab.com/carletonblueprint/beneficent/commit/a3cc837bb1105b7be0abe863713c543cbc13316d))
* update app on client creation (EN-339) ([33cab68](https://gitlab.com/carletonblueprint/beneficent/commit/33cab68e7eb768806fcab9370fe104c41c8ca788))

## [4.1.0](https://gitlab.com/carletonblueprint/beneficent/compare/v4.0.0...v4.1.0) (2021-08-04)


### Features

* change file downloads ([4e42092](https://gitlab.com/carletonblueprint/beneficent/commit/4e420929ecf25aef0f7f6d02d3ccaf49a0ed996e))


### Bug Fixes

* move seeding to util in cli (EN-274) ([0badbf9](https://gitlab.com/carletonblueprint/beneficent/commit/0badbf9b88211717319c61cd3823c21e14db4be7))
* remove redundant docker configs (EN-356) ([201a595](https://gitlab.com/carletonblueprint/beneficent/commit/201a595691ebef1197705d5cdee7b86524114780))

## [4.0.0](https://gitlab.com/carletonblueprint/beneficent/compare/v3.0.3...v4.0.0) (2021-07-28)


### ⚠ BREAKING CHANGES

* change app validation to not require guarantor info (EN-320)
* contract payment calculations (EN-330)

### Features

* add create and edit contract form components and fix authentication bugs (EN-267) ([4892516](https://gitlab.com/carletonblueprint/beneficent/commit/48925169461d1bfe61f6a7d278aa7884262c8ca2))
* add guarantor info (EN-320) ([5e1ecfc](https://gitlab.com/carletonblueprint/beneficent/commit/5e1ecfc6d2060a5456ac94611d2acd19adf9b5c3))
* create upload api (EN-332) ([7c062c6](https://gitlab.com/carletonblueprint/beneficent/commit/7c062c62f01dc46bfa70ce62fe99736ba770f061))


### Bug Fixes

* change app validation to not require guarantor info (EN-320) ([3bda4c4](https://gitlab.com/carletonblueprint/beneficent/commit/3bda4c41c9eb7e23c8b0ef6555fa5f34738c669e))
* contract payment calculations (EN-330) ([e3c3b2d](https://gitlab.com/carletonblueprint/beneficent/commit/e3c3b2dee2cb24fe4ce8e55f6fffea6727955637))

### [3.0.3](https://gitlab.com/carletonblueprint/beneficent/compare/v3.0.2...v3.0.3) (2021-07-21)


### Bug Fixes

* enable auto deploy pipeline (EN-350) ([cc98dcd](https://gitlab.com/carletonblueprint/beneficent/commit/cc98dcd0d728e3f026cab4c2da1dec0ffc4f14f3))

### [3.0.2](https://gitlab.com/carletonblueprint/beneficent/compare/v3.0.1...v3.0.2) (2021-07-21)


### Bug Fixes

* add describe blocks to test suites (EN-350) ([3264203](https://gitlab.com/carletonblueprint/beneficent/commit/32642038312feb736a324bbae543530ddb855e09))
* add ssh key to after script (EN-350) ([b151ba3](https://gitlab.com/carletonblueprint/beneficent/commit/b151ba376b3894e7034a7a09388a2a7b5466012a))
* auto deploy pipeline (EN-350) ([7049baf](https://gitlab.com/carletonblueprint/beneficent/commit/7049bafed0a8c65157f50169fcc9e96180083b6b))
* auto deploy pipeline (EN-350) ([412f44c](https://gitlab.com/carletonblueprint/beneficent/commit/412f44c34c617034c1bc5522c9efb53b46ff2243))
* fast forward only merge on auto deploy pipeline (EN-350) ([c69bbf7](https://gitlab.com/carletonblueprint/beneficent/commit/c69bbf706281e3037358d2ddbd291a97fc8f94ce))
* output junit test report (EN-350) ([abea3d2](https://gitlab.com/carletonblueprint/beneficent/commit/abea3d26f855828bd04bf7b76c313639c8e7619d))

### [3.0.1](https://gitlab.com/carletonblueprint/beneficent/compare/v3.0.0...v3.0.1) (2021-07-19)


### Bug Fixes

* application user query (EN-330) ([a1f21eb](https://gitlab.com/carletonblueprint/beneficent/commit/a1f21ebaac38d29ac5a8cf613d9f9d99e6a0bd0e))
* increase branch coverage (EN-348) ([d0c9f13](https://gitlab.com/carletonblueprint/beneficent/commit/d0c9f1394d92ad179f78d2376eb88fec19e88611))

## [3.0.0](https://gitlab.com/carletonblueprint/beneficent/compare/v2.1.1...v3.0.0) (2021-07-19)


### ⚠ BREAKING CHANGES

* implement downloads for contracts (EN-331)
* refactor auth and user logic (EN-335)

### Features

* add api to get app version (EN-322) ([f7ec5e2](https://gitlab.com/carletonblueprint/beneficent/commit/f7ec5e2fbde89d602264f435b7dfcb1ef3324950))
* add debug configs (EN-336) ([3605c6a](https://gitlab.com/carletonblueprint/beneficent/commit/3605c6a4d48c629806ba3ce3adee7f09775d9fbf))
* add nodemon support for backend (EN-303) ([5dcf00a](https://gitlab.com/carletonblueprint/beneficent/commit/5dcf00aab0978805cce5d007a12c5ea4e5e99aaf))
* add status validation for application endpoints (EN-270) ([62d543a](https://gitlab.com/carletonblueprint/beneficent/commit/62d543ae86f5f98f67f654338abc3b67601fa4d0))
* implement downloads for contracts (EN-331) ([f6f12cc](https://gitlab.com/carletonblueprint/beneficent/commit/f6f12cc54f9a41cab6133254b17178e88eaf10c1))
* update contract api (EN-330) ([8b747d9](https://gitlab.com/carletonblueprint/beneficent/commit/8b747d9fbc71ac7f2ef246e1b2e9faf2ff57e9fe))


### Bug Fixes

* allow multiple query params (EN-277) ([3fc6a7c](https://gitlab.com/carletonblueprint/beneficent/commit/3fc6a7ccc648cf8ddfab0e3863168ecd833e2128))
* refactor auth and user logic (EN-335) ([65bcd6d](https://gitlab.com/carletonblueprint/beneficent/commit/65bcd6d93e7f1baec0501598808586e3f3993a17))
* update enum and allow empty strings for statuses in get applications (EN-270) ([83eebb7](https://gitlab.com/carletonblueprint/beneficent/commit/83eebb759d5fe88f43db365c276fd791206c5098))
* update lock file with new packages (EN-331) ([e6c4f39](https://gitlab.com/carletonblueprint/beneficent/commit/e6c4f3934436b1727c915399f32e445849b39e85))

### [2.1.1](https://gitlab.com/carletonblueprint/beneficent/compare/v2.1.0...v2.1.1) (2021-07-12)


### Bug Fixes

* use sed to fix filepaths in coverage reports (EN-321) ([92e9850](https://gitlab.com/carletonblueprint/beneficent/commit/92e98500d9ba75da046899b801a75001304b66ea))

## [2.1.0](https://gitlab.com/carletonblueprint/beneficent/compare/v2.0.1...v2.1.0) (2021-07-12)


### Features

* add email validation (EN-306) ([178d21b](https://gitlab.com/carletonblueprint/beneficent/commit/178d21bf3acf3f3401caef38c1324206fbedf77a))
* add test coverage visualization (EN-321) ([556f6f0](https://gitlab.com/carletonblueprint/beneficent/commit/556f6f02cfeb00aeaa5a01932bee11520228a4bc))
* migrate past beneficent data (EN-288) ([fb06c05](https://gitlab.com/carletonblueprint/beneficent/commit/fb06c05c37c39f8d358d73d73bba50b1643bb782))

### [2.0.1](https://gitlab.com/carletonblueprint/beneficent/compare/v2.0.0...v2.0.1) (2021-07-05)


### Bug Fixes

* update loan officer workload calculation (EN-281) ([84bdba4](https://gitlab.com/carletonblueprint/beneficent/commit/84bdba4847bc84b7347afacda2b96d2767d5aab3))

## [2.0.0](https://gitlab.com/carletonblueprint/beneficent/compare/v0.2.0...v2.0.0) (2021-06-28)


### ⚠ BREAKING CHANGES

* add util service and clean command (EN-298)
* add application fields (EN-282)
* refactor searching logic (EN-297)

### Features

* add util service and clean command (EN-298) ([81fded2](https://gitlab.com/carletonblueprint/beneficent/commit/81fded2b3f70f731b5004bf2d4e60143fb07061d))
* add versions to all services (EN-296) ([46caf00](https://gitlab.com/carletonblueprint/beneficent/commit/46caf00e496c3ef403a01ea7fe108381b2785ef8))
* refactor searching logic (EN-297) ([6a949a8](https://gitlab.com/carletonblueprint/beneficent/commit/6a949a84a263ef41d1508f6b5da43196b6088ef3))


### Bug Fixes

* add application fields (EN-282) ([03f3e2f](https://gitlab.com/carletonblueprint/beneficent/commit/03f3e2f30efea964bc77098cc61e9b05fa810697))

## [1.1.0](https://gitlab.com/carletonblueprint/beneficent/compare/v0.2.0...v1.1.0) (2021-06-21)


### Features

* add versions to all services (EN-296) ([ff385af](https://gitlab.com/carletonblueprint/beneficent/commit/ff385afab403b5eeb35f6d08248e7459deebf8a2))

## [1.0.0](https://gitlab.com/carletonblueprint/beneficent/compare/v0.1.1...v0.2.0) (2021-06-21)


### ⚠ BREAKING CHANGES

* feat add first and last name to models (EN-276)

### Features

* add first and last name filtering (EN-276) ([80a2c86](https://gitlab.com/carletonblueprint/beneficent/commit/80a2c86d4792fec218e2cc8b6229a88b8c25024c))
* add first and last name to docs (EN-276) ([986d316](https://gitlab.com/carletonblueprint/beneficent/commit/986d316cd9d71721081587d9f5f1a03802aca484))
* add first and last name to validations (EN-276) ([f97b921](https://gitlab.com/carletonblueprint/beneficent/commit/f97b92137d94fe3a1ce191697a57e3c02e33ec98))
* auto fix eslint and prettier issues on commit (EN-293) ([a26045d](https://gitlab.com/carletonblueprint/beneficent/commit/a26045d70011d9565ec28ea92afc45ede31bb305))
* feat add first and last name to models (EN-276) ([51f646f](https://gitlab.com/carletonblueprint/beneficent/commit/51f646f8399965d16e92516f7fcd2df309bc3ae1))
* update database seed file (EN-276) ([50d09d0](https://gitlab.com/carletonblueprint/beneficent/commit/50d09d083f960c26c88880dc3f6464095c4f38f5))
* update swagger documentation for routes (EN-276) ([3824ee3](https://gitlab.com/carletonblueprint/beneficent/commit/3824ee3333716fa7dc3ffb71aa4c9e75aa8d904f))


### Bug Fixes

* add files to commit after formatting (EN-295) ([bf3aad3](https://gitlab.com/carletonblueprint/beneficent/commit/bf3aad3f0e85db73035b14e1a6ec467864e0d1f0))
* remove pattern match from prettier command (EN-293) ([f48602e](https://gitlab.com/carletonblueprint/beneficent/commit/f48602ee936bb7dc37857ab280a071fcf2997d56))
* update sorting test (EN-276) ([83e0efd](https://gitlab.com/carletonblueprint/beneficent/commit/83e0efd610ef4791caa24318d9eaaa48e00add28))
* update tests to reflect name splitting (EN-276) ([1b0be6f](https://gitlab.com/carletonblueprint/beneficent/commit/1b0be6f68e80d934820a38a114ac4fac6e376590))

### [0.1.0](https://gitlab.com/carletonblueprint/beneficent/compare/v0.0.2...v0.1.1) (2021-06-17)


### Features

* add git commit helper to cli (EN-289) ([93149f2](https://gitlab.com/carletonblueprint/beneficent/commit/93149f2f604d98d2e4482c80e5b6d95b46b6062d))
* add husky and lint-staged to enforce style standards (EN-286) ([7766d277](https://gitlab.com/carletonblueprint/beneficent/-/commit/42cd2c8f62fea726e0bbc136ad5df05661c710c5))
* add client filtering by date signed (EN-277) ([c16376a3](https://gitlab.com/carletonblueprint/beneficent/-/commit/c16376a3a7de97bc1c45dbdc7e3ca18c883239a8))
* add changelog and versioning (EN-275) ([09bd4f2f](https://gitlab.com/carletonblueprint/beneficent/-/commit/09bd4f2fd0875d88d36fd88ea5452abb19709d20))
* add create cli tool for project (EN-274) ([e5a94408](https://gitlab.com/carletonblueprint/beneficent/-/commit/e5a9440877d476ae441f8b863e8016c9231f48bb))
* add backend validation (EN-262) ([e5a94408](https://gitlab.com/carletonblueprint/beneficent/-/commit/d736e5d6b468a5a62ef255226aeede5a7c7ce314))
* create a file to seed db (EN-253) ([e5a94408](https://gitlab.com/carletonblueprint/beneficent/-/commit/2f926b17e34daa94793e3ab717d0aa682ade57c5))

### Bug Fixes

* update swagger typo (EN-283) ([709ed81](https://gitlab.com/carletonblueprint/beneficent/commit/709ed8128930a46805239ef8fa3a567dfedb75e4))
* refactor frontend structure (EN-252) ([99dc846d](https://gitlab.com/carletonblueprint/beneficent/-/commit/99dc846d8b2d92cc701a4029c307e07203a15d6e))
* change password reset message (EN-248) ([37a841c8](https://gitlab.com/carletonblueprint/beneficent/-/commit/563ae5c678eb5981b378bc67ed708d1e51e6ae46))
