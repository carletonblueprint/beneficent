const mongoose = require("mongoose");
const faker = require("faker");
const Client = require("../../src/models/client.model");
const { officerOne, officerTwo } = require("./officer.fixture");
const { applicationOne, applicationTwo } = require("./application.fixture");

const dateOne = new Date("2021-01-31");
const dateTwo = new Date("2021-03-31");

const clientOne = {
  _id: mongoose.Types.ObjectId(),
  dateSigned: dateOne,
  officer: officerOne._id,
  application: applicationOne._id
};

const clientTwo = {
  _id: mongoose.Types.ObjectId(),
  dateSigned: dateTwo,
  officer: officerTwo._id,
  application: applicationTwo._id
};

const insertClients = async (clients) => {
  await Client.insertMany(clients.map((client) => ({ ...client })));
};

module.exports = {
  dateOne,
  dateTwo,
  clientOne,
  clientTwo,
  insertClients
};
