const httpStatus = require("http-status");
const { Client, Application } = require("../models");
const ApiError = require("../utils/ApiError");
const { validateUserId, validateOfficerId, validateApplicationId } = require("../utils/serviceUtils");
const applicationService = require("./application.service");

/**
 * Create a client
 * @param {Object} clientBody
 * @returns {Promise<Client>}
 */
const createClient = async (clientBody) => {
  await validateOfficerId(clientBody.officer, "Officer with this ID does not exist");
  await validateApplicationId(clientBody.application, "Application with this ID does not exist");
  if (await Client.isDuplicate(clientBody.application)) {
    throw new ApiError(httpStatus.BAD_REQUEST, "Client with this application ID already exists");
  }
  return Client.create(clientBody);
};

/**
 * Query for clients
 * @param {Object} filter - Mongo filter
 * @param {Object} options - Query options
 * @param {string} [options.populate] - Populate data fields. Hierarchy of fields should be separated by (.). Multiple populating criteria should be separated by commas (,)
 * @param {string} [options.sortBy] - Sort option in the format: sortField:(desc|asc)
 * @param {number} [options.limit] - Maximum number of results per page (default = 10)
 * @param {number} [options.page] - Current page (default = 1)
 * @returns {Promise<QueryResult>}
 */
const queryClients = async (filter, options) => {
  let clientFilter = {};
  let applicationFilter = { $and: [{ $or: [] }] };

  if (filter.nameOrEmail) {
    applicationFilter.$and[0].$or.push({ firstName: { $regex: filter.nameOrEmail, $options: "i" } });
    applicationFilter.$and[0].$or.push({ lastName: { $regex: filter.nameOrEmail, $options: "i" } });
    applicationFilter.$and[0].$or.push({ email: { $regex: filter.nameOrEmail, $options: "i" } });
  }

  if (applicationFilter.$and[0].$or.length === 0) applicationFilter.$and.shift();
  if (applicationFilter.$and.length === 0) delete applicationFilter.$and;

  //use applications to filter?
  const applications = await Application.find(applicationFilter);

  let ids = [];
  applications.forEach((application) => {
    ids.push(application.id);
  });
  clientFilter = {
    $and: [{ application: { $in: ids } }],
    $or: []
  };
  if (filter.officer) clientFilter.$or.push({ officer: filter.officer });
  clientFilter.$and.push({
    dateSigned: {
      $gte: filter.startDate ? filter.startDate : new Date("January 1, 1970 00:00:01"),
      $lte: filter.endDate ? filter.endDate : Date.now()
    }
  });

  if (clientFilter.$or.length === 0) delete clientFilter.$or;

  const clients = await Client.paginate(clientFilter, options);
  if (clients.results.length == 0) {
    throw new ApiError(httpStatus.NOT_FOUND, "No clients found");
  }
  return clients;
};

/**
 * Get client by id
 * @param {ObjectId} clientId
 * @returns {Promise<Client>}
 */
const getClientById = async (clientId) => {
  return Client.findById(clientId);
};

/**
 * Get client by user id
 * @param {ObjectId} userId
 * @returns {Promise<Client>}
 */
const getClientByUserId = async (userId) => {
  const application = await Application.findOne({ user: userId });
  if (application) {
    return Client.findOne({ application: application._id });
  } else {
    return undefined;
  }
};

/**
 * Update client by id
 * @param {ObjectId} clientId
 * @param {Object} updateBody
 * @returns {Promise<Client>}
 */
const updateClientById = async (clientId, updateBody) => {
  const client = await getClientById(clientId);
  if (!client) {
    throw new ApiError(httpStatus.NOT_FOUND, "Client not found");
  }

  await validateOfficerId(updateBody.officer, "Officer with this ID does not exist");

  Object.assign(client, updateBody);
  await client.save();
  return client;
};

/**
 * Delete client by id
 * @param {ObjectId} clientId
 * @returns {Promise<Client>}
 */
const deleteClientById = async (clientId) => {
  const client = await getClientById(clientId);
  if (!client) {
    throw new ApiError(httpStatus.NOT_FOUND, "Client not found");
  }
  await client.remove();
  return client;
};

module.exports = {
  createClient,
  queryClients,
  getClientById,
  getClientByUserId,
  updateClientById,
  deleteClientById
};
