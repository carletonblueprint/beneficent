const Joi = require("joi");
const { objectId } = require("./custom.validation");

const createClient = {
  body: Joi.object().keys({
    dateSigned: Joi.date().required(),
    currentContract: Joi.string().custom(objectId),
    application: Joi.string().required().custom(objectId),
    officer: Joi.string().required().custom(objectId)
  })
};

const getClients = {
  query: Joi.object().keys({
    nameOrEmail: Joi.string(),
    startDate: Joi.date(),
    endDate: Joi.date(),
    officer: [Joi.string().custom(objectId), Joi.array().items(Joi.string().custom(objectId))],
    populate: Joi.string(),
    sortBy: Joi.string(),
    limit: Joi.number().integer(),
    page: Joi.number().integer()
  })
};

const getClient = {
  params: Joi.object().keys({
    clientId: Joi.required().custom(objectId)
  })
};

const updateClient = {
  params: Joi.object().keys({
    clientId: Joi.required().custom(objectId)
  }),
  body: Joi.object()
    .keys({
      contract: Joi.string().custom(objectId),
      officer: Joi.string().custom(objectId)
    })
    .min(1)
};

const deleteClient = {
  params: Joi.object().keys({
    clientId: Joi.required().custom(objectId)
  })
};

module.exports = {
  createClient,
  getClients,
  getClient,
  updateClient,
  deleteClient
};
