const mongoose = require("mongoose");
const faker = require("faker");
const fs = require("fs");
const { Contract } = require("../../src/models");
const { clientOne, clientTwo } = require("./client.fixture");

const dateOne = new Date("2021-01-01");
const dateTwo = new Date("2022-01-01");
const dateThree = new Date("2001-01-01");

const contractOne = {
  _id: mongoose.Types.ObjectId(),
  guarantorName: faker.name.findName(),
  approvedLoanAmount: 1500,
  firstPaymentDue: dateOne,
  finalPaymentDue: dateTwo,
  status: "Draft",
  contractStartDate: dateThree,
  client: clientOne._id
};

const contractTwo = {
  _id: mongoose.Types.ObjectId(),
  guarantorName: faker.name.findName(),
  approvedLoanAmount: 3000,
  firstPaymentDue: dateOne,
  finalPaymentDue: dateTwo,
  status: "Draft",
  contractStartDate: dateThree,
  client: clientTwo._id
};

const insertContracts = async (contracts) => {
  await Contract.insertMany(contracts);
};

const secondsInMonth = 2628000000;

/**
 * @param {Number} approvedAmount
 * @param {Number} finalPaymentTime - Unix time
 * @param {Number} firstPaymentTime - Unix time
 * @returns {Number} - Monthly payment rounded to the nearest 5
 */
const calculateMonthlyPayment = (approvedAmount, finalPaymentTime, firstPaymentTime) => {
  // Approved amount divided by number of months (rounded to the nearest month), rounded to nearest 5
  return Math.ceil(approvedAmount / Math.round((finalPaymentTime - firstPaymentTime) / secondsInMonth) / 5) * 5;
};

/**
 * @param {Number} approvedAmount
 * @param {Number} monthlyPayment
 * @param {Number} finalPaymentTime - Unix time
 * @param {Number} firstPaymentTime - Unix time
 * @returns {Number} - Final payment after (# of months in term - 1) monthly payments have been made
 */
const calculateFinalPayment = (approvedAmount, monthlyPayment, finalPaymentTime, firstPaymentTime) => {
  // The remaining amount to pay after all but one month has been payed off at the monthly payment rate
  // ((finalPaymentTime - firstPaymentTime - secondsInMonth) / secondsInMonth) = # of months in payment term - 1 month
  return (
    approvedAmount - monthlyPayment * Math.round((finalPaymentTime - firstPaymentTime - secondsInMonth) / secondsInMonth)
  );
};

/**
 * @param {Number} approvedAmount
 * @param {Number} monthlyPayment
 * @param {Number} finalPaymentTime - Unix time
 * @param {Number} firstPaymentTime - Unix time
 * @returns - Updated final payment after (# of months in term - 1) monthly payments have been made
 */
const calculateNewFinalPayment = (approvedAmount, monthlyPayment, finalPaymentTime, firstPaymentTime) => {
  // approvedAmount, finalPaymentTime and firstPaymentTime are chosen based on whether or not
  // They were provided in the updateBody, otherwise, they are taken from the contract
  // monthlyPayment will always either be sent with the updateBody or calculated

  // The remaining amount to pay after all but one month has been payed off at the monthly payment rate
  // ((finalPaymentTime - firstPaymentTime - secondsInMonth) / secondsInMonth) = # of months in payment term - 1 month

  const finalPayment =
    approvedAmount - monthlyPayment * Math.round((finalPaymentTime - firstPaymentTime - secondsInMonth) / secondsInMonth);

  // There may be cases where the new final payment calculated results in the loan being paid off earlier than required. In that case, return the final
  // amount that would be paid without exceeding the loan
  return finalPayment >= 0 ? finalPayment : monthlyPayment + finalPayment;
};

module.exports = {
  dateOne,
  dateTwo,
  dateThree,
  contractOne,
  contractTwo,
  insertContracts,
  calculateMonthlyPayment,
  calculateFinalPayment,
  calculateNewFinalPayment
};
