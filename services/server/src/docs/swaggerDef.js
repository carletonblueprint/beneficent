const { version } = require("../../package.json");
const config = require("../config/config");

const swaggerDef = {
  openapi: "3.0.0",
  info: {
    title: "Beneficent API",
    version
  },
  servers: [
    {
      url: config.apiEndpoint + "/api/v1"
    }
  ]
};

module.exports = swaggerDef;
